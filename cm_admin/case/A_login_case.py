# coding=utf-8
from seleniumtest.common.myunit import StartEnd
from seleniumtest.caimei.cm_admin.business.A_login_business import LoginBusiness
from seleniumtest.common.function import OperationMethod as om
from seleniumtest.util.operation_json import OperationJson
from time import sleep

class LoginCase(StartEnd):

    def test_A01_login_page(self):
        '''
        检查‘登录‘功能是否正常
        :return:
        '''
        self.lb = LoginBusiness(self.driver)
        self.oj = OperationJson()
        self.lb.login('operate')
        sleep(4)
        om().screen_img('cm_admin', self.driver, 'A01_login_page.png')
        self.assertEqual(self.lb.login(), '您好, 系统管理员 ')