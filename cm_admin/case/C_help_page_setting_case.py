# coding = utf-8
from seleniumtest.common.myunit import StartEnd
from seleniumtest.caimei.cm_admin.business.C_help_page_setting_business import HelpPageSettingBusiness
from seleniumtest.common.function import OperationMethod as om
import random

class HelpPageSettingCase(StartEnd):
    def test_C01_help_setting_page(self):
        '''
        检查进入‘帮助页设置’页面功能是否正常
        :return:
        '''
        self.hpsb = HelpPageSettingBusiness(self.driver)
        self.hpsb.help_page_settings('operate')
        om().screen_img('cm_admin', self.driver, 'C01_help_setting_page.png')
        self.driver.switch_to.frame('mainFrame')
        self.assertEqual(self.hpsb.help_page_settings(), '添加帮助页')

    def test_C02_help_setting_page(self):
        '''
        检查'查询功能'是否正常
        :return:
        '''
        self.hpsb = HelpPageSettingBusiness(self.driver)
        self.hpsb.help_page_query('operate')
        om().screen_img('cm_admin', self.driver, 'C02_help_setting_page.png')
        self.assertEqual(self.hpsb.help_page_query(), '账号注册')

    def test_C03_help_setting_page(self):
        '''
        检查'更新状态'功能是否正常
        :return:
        '''
        self.hpsb = HelpPageSettingBusiness(self.driver)
        self.hpsb.help_page_update_status('operate')
        om().screen_img('cm_admin', self.driver, 'C03_help_setting_page.png')

    def test_C04_help_setting_page(self):
        '''
        检查'更新排名'功能是否正常
        :return:
        '''
        sort = str(random.randint(0, 100))
        self.hpsb = HelpPageSettingBusiness(self.driver)
        self.hpsb.help_page_sort('operate', sort)
        om().screen_img('cm_admin', self.driver, 'C04_help_setting_page.png')

    # def test_C05_help_setting_page(self):
    #     '''
    #     检查'编辑'功能是否正常
    #     :return:
    #     '''
    #     sort = str(random.randint(0, 100))
    #     self.hpsb = HelpPageSettingBusiness(self.driver)
    #     self.hpsb.help_page_edit('operate', '帐户管理' + sort, '测试添加帮助页内容' + sort)
    #     self.assertEqual(self.hpsb.help_page_edit(), '帐户管理' + sort)
    #     om().screen_img('cm_admin', self.driver, 'C05_help_setting_page.png')
    #
    # def test_C06_help_setting_page(self):
    #     '''
    #     检查'添加帮助页'功能是否正常
    #     :return:
    #     '''
    #     sort = str(random.randint(0, 100))
    #     self.hpsb = HelpPageSettingBusiness(self.driver)
    #     self.hpsb.add_help_page('operate', '测试帮助页' + sort, '这是测试添加帮助页内容哈哈哈哈哈' + sort)
    #     self.assertEqual(self.hpsb.add_help_page(), '测试帮助页' + sort)
    #     om().screen_img('cm_admin', self.driver, 'C06_help_setting_page.png')
