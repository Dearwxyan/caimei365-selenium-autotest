# coding = utf-8
from seleniumtest.common.myunit import StartEnd
from seleniumtest.caimei.cm_admin.business.B_geographical_setting_business import GeographicalSettingBusiness
from seleniumtest.common.function import OperationMethod as om
from time import sleep
import random

class GeographicalSettingCase(StartEnd):
    def test_B01_geographical_setting_page(self):
        '''
        检查进入‘地域设置’页面功能是否正常
        :return:
        '''
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.geographical_setting('operate')
        sleep(4)
        om().screen_img('cm_admin', self.driver, 'B01_geographical_setting_page.png')
        self.driver.switch_to.frame('mainFrame')
        self.assertEqual(self.gsb.geographical_setting(), '添加省份')

    def test_B02_geographical_setting_page(self):
        '''
        检查‘添加省份’功能是否正常
        :return:
        '''
        num = str(random.randint(0, 1000))
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.add_provinces('operate', '广东省'+ num)
        sleep(4)
        om().screen_img('cm_admin', self.driver, 'B02_geographical_setting_page.png')
        self.assertEqual(self.gsb.add_provinces(), '广东省' + num)

    def test_B03_geographical_setting_page(self):
        '''
        检查‘编辑省份’功能是否正常
        :return:
        '''
        num = str(random.randint(0, 1000))
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.edit_provinces('operate', '北京市'+ num)
        om().screen_img('cm_admin', self.driver, 'B03_geographical_setting_page.png')
        self.assertEqual(self.gsb.edit_provinces(), '北京市' + num)

    def test_B04_geographical_setting_page(self):
        '''
        检查点击省份设置‘更新状态’功能是否正常
        :return:
        '''
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.provinces_update_status('operate')
        self.gsb.provinces_update_status()
        om().screen_img('cm_admin', self.driver, 'B04_geographical_setting_page.png')

    def test_B05_geographical_setting_page(self):
        '''
        检查进入‘城市设置’页面功能是否正常
        :return:
        '''
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.city_setting('operate')
        om().screen_img('cm_admin', self.driver, 'B05_geographical_setting_page.png')
        self.assertEqual(self.gsb.city_setting(), '添加城市')

    def test_B06_geographical_setting_page(self):
        '''
        检查’添加城市‘功能是否正常
        :return:
        '''
        num = str(random.randint(0, 1000))
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.add_city('operate', '湛江市' + num)
        om().screen_img('cm_admin', self.driver, 'B06_geographical_setting_page.png')
        self.assertEqual(self.gsb.add_city(), '湛江市' + num)

    def test_B07_geographical_setting_page(self):
        '''
        检查’编辑城市‘功能是否正常
        :return:
        '''
        num = str(random.randint(0, 1000))
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.edit_city('operate', '市辖区' + num)
        om().screen_img('cm_admin', self.driver, 'B07_geographical_setting_page.png')
        self.assertEqual(self.gsb.edit_city(), '市辖区' + num)

    def test_B08_geographical_setting_page(self):
        '''
        检查点击城市设置‘更新状态’功能是否正常
        :return:
        '''
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.city_update_status('operate')
        self.gsb.city_update_status()
        om().screen_img('cm_admin', self.driver, 'B08_geographical_setting_page.png')

    def test_B09_geographical_setting_page(self):
        '''
        检查进入‘区域设置’页面功能是否正常
        :return:
        '''
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.regional_setting('operate')
        om().screen_img('cm_admin', self.driver, 'B09_geographical_setting_page.png')
        self.assertEqual(self.gsb.regional_setting(), '添加区域')

    def test_B10_geographical_setting_page(self):
        '''
        检查’添加区域‘功能是否正常
        :return:
        '''
        regiona_num = str(random.randint(0, 1000))
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.add_regional('operate', '霞山区' + regiona_num, '010'+ regiona_num, '101' + regiona_num )
        om().screen_img('cm_admin', self.driver, 'B10_geographical_setting_page.png')
        self.assertEqual(self.gsb.add_regional('assert'), '霞山区' + regiona_num)
        self.assertEqual(self.gsb.add_regional('assert1'), '010' + regiona_num)
        self.assertEqual(self.gsb.add_regional('assert2'), '101' + regiona_num)

    def test_B11_geographical_setting_page(self):
        '''
        检查’编辑区域‘功能是否正常
        :return:
        '''
        regiona_num = str(random.randint(0, 1000))
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.edit_regional('operate', '东城区' + regiona_num, '010'+ regiona_num, '101' + regiona_num )
        om().screen_img('cm_admin', self.driver, 'B11_geographical_setting_page.png')
        self.assertEqual(self.gsb.edit_regional('assert'), '东城区' + regiona_num)
        self.assertEqual(self.gsb.edit_regional('assert1'), '010' + regiona_num)
        self.assertEqual(self.gsb.edit_regional(), '101' + regiona_num)

    def test_B12_geographical_setting_page(self):
        '''
        检查点击区域设置‘更新状态’功能是否正常
        :return:
        '''
        self.gsb = GeographicalSettingBusiness(self.driver)
        self.gsb.regional_update_status('operate')
        self.gsb.regional_update_status()
        om().screen_img('cm_admin', self.driver, 'B12_geographical_setting_page.png')
