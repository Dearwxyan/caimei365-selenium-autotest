# coding = utf-8
from seleniumtest.caimei.cm_admin.handle.B_geographical_setting_handle import GeographicalSettingHandle
from seleniumtest.caimei.cm_admin.business.A_login_business import LoginBusiness

class GeographicalSettingBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.gsh = GeographicalSettingHandle(self.driver)
        self.lg = LoginBusiness(self.driver)

    # 操作进入‘地域设置’页面功能
    def geographical_setting(self, action=None):
        try:
            if action == 'operate':
                # self.read_cookies()
                self.lg.lh.lp.fe.open()
                self.lg.common_login()
                self.gsh.geographical_setting_element('geographical_setting')
            else:
                return self.gsh.geographical_setting_element()
        except ValueError:
            print('操作进入‘地域设置’页面功能失败！')

    # 操作‘添加省份’功能
    def add_provinces(self, action=None, province_name=None):
        try:
            if action == 'operate':
                self.geographical_setting('operate')
                self.driver.switch_to.frame('mainFrame')
                self.gsh.provinces_element('add_provinces')
                self.gsh.provinces_element('provinces_pop_up_x')
                self.gsh.provinces_element('add_provinces')
                self.gsh.provinces_element('province_name_input', province_name)
                self.gsh.provinces_element('province_ok_button')
            else:
                self.gsh.next_page()
                return self.gsh.provinces_element('add_provinces_assert')
        except ValueError:
            print('操作‘添加省份’功能失败！')

    # 操作‘编辑省份’功能
    def edit_provinces(self, action=None, province_name=None):
        try:
            if action == 'operate':
                self.geographical_setting('operate')
                self.driver.switch_to.frame('mainFrame')
                self.gsh.provinces_element('edit_porprovince')
                self.gsh.provinces_element('provinces_pop_up_x')
                self.gsh.provinces_element('edit_porprovince')
                self.gsh.provinces_element('province_name_input', province_name)
                self.gsh.provinces_element('province_ok_button')
            else:
                return self.gsh.provinces_element()
        except ValueError:
            print('操作‘添加省份’功能失败！')

    # 操作省份设置’更新状态‘功能
    def provinces_update_status(self, action=None):
        try:
            if action == 'operate':
                self.geographical_setting('operate')
                self.driver.switch_to.frame('mainFrame')
                self.gsh.provinces_update_status_element('provinces_update_status')
                self.gsh.provinces_update_status_element('provinces_update_status_x')
                self.gsh.provinces_update_status_element('provinces_update_status')
            else:
                self.gsh.provinces_update_status_element()
        except ValueError:
            print('操作省份设置’更新状态‘功能失败！')

    # 操作进入‘城市设置’页面功能
    def city_setting(self, acton=None):
        try:
            if acton == 'operate':
                self.geographical_setting('operate')
                self.driver.switch_to.frame('mainFrame')
                self.gsh.urban_setting_element('city_setting')
            else:
                return self.gsh.urban_setting_element()
        except ValueError:
            print('操作进入‘城市设置’页面功能失败！')

    # 操作’添加城市‘功能
    def add_city(self, action=None, city_name=None):
        try:
            if action == 'operate':
                self.city_setting('operate')
                self.gsh.city_element('add_city')
                self.gsh.city_element('city_pop_up_x')
                self.gsh.city_element('add_city')
                self.gsh.city_element('city_name_input', city_name)
                self.gsh.city_element('city_ok_button')
            else:
                self.gsh.next_page()
                return self.gsh.city_element('add_city_assert')
        except ValueError:
            print('操作’添加城市‘功能失败！')

    # 操作’编辑城市‘功能
    def edit_city(self, action=None, city_name=None):
        try:
            if action == 'operate':
                self.city_setting('operate')
                self.gsh.city_element('edit_city')
                self.gsh.city_element('city_pop_up_x')
                self.gsh.city_element('edit_city')
                self.gsh.city_element('city_name_input', city_name)
                self.gsh.city_element('city_ok_button')
            else:
                return self.gsh.city_element('edit_city_assert')
        except ValueError:
            print('操作’编辑城市‘功能失败！')

    # 操作城市设置’更新状态‘功能
    def city_update_status(self, action=None):
        try:
            if action == 'operate':
                self.city_setting('operate')
                self.gsh.city_update_status_element('city_update_status')
                self.gsh.city_update_status_element('city_update_status_x')
                self.gsh.city_update_status_element('city_update_status')
            else:
                self.gsh.city_update_status_element()
        except ValueError:
            print('操作城市设置’更新状态‘功能失败！')

    # 操作进入‘区域设置’页面功能
    def regional_setting(self, acton=None):
        try:
            if acton == 'operate':
                self.city_setting('operate')
                self.gsh.regional_setting_element('regional_setting')
            else:
                return self.gsh.regional_setting_element()
        except ValueError:
            print('操作进入‘城市设置’页面功能失败！')

    # 操作’添加区域‘功能
    def add_regional(self, action=None, regional_name=None, postal_code=None, area_number=None):
        try:
            if action == 'operate':
                self.regional_setting('operate')
                self.gsh.regional_element('add_regional')
                self.gsh.regional_element('regional_pop_up_x')
                self.gsh.regional_element('add_regional')
                self.gsh.regional_element('regional_name_input', regional_name=regional_name)
                self.gsh.regional_element('regional_postal_number_input', postal_code=postal_code)
                self.gsh.regional_element('regional_area_number_input', area_number=area_number)
                self.gsh.regional_element('regional_ok_button')
            elif action == 'assert':
                self.gsh.next_page()
                return self.gsh.regional_element('add_regional_assert')
            elif action == 'assert1':
                self.gsh.next_page()
                return self.gsh.regional_element('add_regional_assert1')
            else:
                self.gsh.next_page()
                return self.gsh.regional_element('add_regional_assert2')
        except ValueError:
            print('操作’添加区域‘功能失败！')

    # 操作‘编辑区域‘功能
    def edit_regional(self, action=None, regional_name=None, postal_code=None, area_number=None):
        try:
            if action == 'operate':
                self.regional_setting('operate')
                self.gsh.regional_element('edit_regional')
                self.gsh.regional_element('regional_pop_up_x')
                self.gsh.regional_element('edit_regional')
                self.gsh.regional_element('regional_name_input', regional_name=regional_name)
                self.gsh.regional_element('regional_postal_number_input', postal_code=postal_code)
                self.gsh.regional_element('regional_area_number_input', area_number=area_number)
                self.gsh.regional_element('regional_ok_button')
            elif action == 'assert':
                return self.gsh.regional_element('edit_regional_assert')
            elif action == 'assert1':
                return self.gsh.regional_element('edit_regional_assert1')
            else:
                return self.gsh.regional_element()
        except ValueError:
            print('操作’编辑区域‘功能失败！')

    # 操作区域设置’更新状态‘功能
    def regional_update_status(self, action=None):
        try:
            if action == 'operate':
                self.regional_setting('operate')
                self.gsh.regional_update_status_element('regional_update_status')
                self.gsh.regional_update_status_element('regional_update_status_x')
                self.gsh.regional_update_status_element('regional_update_status')
            else:
                self.gsh.regional_update_status_element()
        except ValueError:
            print('操作区域设置’更新状态‘功能失败！')
