# coding = utf-8
from time import sleep
from selenium import webdriver
from seleniumtest.caimei.cm_admin.handle.D_search_key_handle import SearchKeyHandle
from seleniumtest.caimei.cm_admin.business.A_login_business import LoginBusiness

class SearchKeyBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.lb = LoginBusiness(driver=self.driver)
        self.skh = SearchKeyHandle(driver=self.driver)

    # 操作进入‘搜索关键字’页面
    def search_key(self):
        try:
            self.lb.common_login()
            self.skh.operate_search_key_page('search_key_menu')
        except:
            print('进入‘搜索关键字’页面失败！')


if __name__ == '__main__':
    driver = webdriver.Chrome()
    driver.maximize_window()
    skb = SearchKeyBusiness(driver)
    skb.search_key()
    sleep(3)
    driver.close()

