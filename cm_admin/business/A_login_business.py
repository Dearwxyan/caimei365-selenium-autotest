# coding=utf-8
from time import sleep
from seleniumtest.caimei.cm_admin.handle.A_login_handle import LoginHandle
from seleniumtest.util.operation_json import OperationJson

class LoginBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.oj = OperationJson()
        self.lh = LoginHandle(self.driver)

    # 操作 ‘登录’功能
    def login(self, action=None, username=None, password=None):
        try:
            if action == 'operate':
                self.lh.lp.fe.open()
                self.lh.login_element('login_name_input', username=self.oj.get_key_value('login', 'mobileOrEmail'))
                self.lh.login_element('login_password_input', password=self.oj.get_key_value('login', 'password'))
                self.lh.login_element('login_button')
            else:
                return self.lh.login_element('login_success_assert')
        except ValueError:
            print('操作 ‘登录’功能失败！')