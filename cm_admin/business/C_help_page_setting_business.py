# coding=utf-8
from seleniumtest.caimei.cm_admin.handle.C_help_page_setting_handle import HelpPageSettingHandle
from seleniumtest.caimei.cm_admin.business.A_login_business import LoginBusiness


class HelpPageSettingBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.hpsh = HelpPageSettingHandle(self.driver)
        self.lg = LoginBusiness(self.driver)

    # 操作进入‘帮助页设置’页面功能
    def help_page_settings(self, action=None):
        try:
            if action == 'operate':
                self.lg.login('operate', 'sysadmin', 'oP8EukQ6G7z')
                self.hpsh.help_page_settings_element('help_page_setting')
            else:
                return self.hpsh.help_page_settings_element()
        except ValueError:
            print('操作进入‘帮助页设置’页面功能失败！')

    # 操作‘查询功能’
    def help_page_query(self, action=None):
        try:
            if action == 'operate':
                self.help_page_settings('operate')
                self.driver.switch_to.frame('mainFrame')
                self.hpsh.help_page_query_element('help_page_information_type')
                self.hpsh.help_page_query_element('choice_beginner_guide')
                self.hpsh.help_page_query_element('help_page_query_button')
            else:
                return self.hpsh.help_page_query_element()
        except ValueError:
            print('操作‘查询功能’失败！')

    # 操作‘更新状态’功能
    def help_page_update_status(self, action=None):
        try:
            if action == 'operate':
                self.help_page_settings('operate')
                self.driver.switch_to.frame('mainFrame')
                self.hpsh.help_page_update_status_element('help_page_update_status')
                self.hpsh.help_page_update_status_element('help_page_x')
                self.hpsh.help_page_update_status_element('help_page_update_status')
                self.hpsh.help_page_update_status_element()
            else:
                print('更新状态成功！！')
        except ValueError:
            print('操作‘更新状态’功能失败！')

    # 操作‘更新状态’功能
    def help_page_sort(self, action=None, sort=None):
        try:
            if action == 'operate':
                self.help_page_settings('operate')
                self.driver.switch_to.frame('mainFrame')
                self.hpsh.help_page_sort_element('help_page_sort_input', sort)
                self.hpsh.help_page_sort_element('help_page_sort_update_ranking')
                self.hpsh.help_page_sort_element('help_page_sort_x')
                self.hpsh.help_page_sort_element('help_page_sort_update_ranking')
                self.hpsh.help_page_sort_element()
            else:
                print('更新排名成功！！')
        except ValueError:
            print('操作‘更新状态’功能失败！')

    # 操作‘编辑’功能
    def help_page_edit(self, action=None, title=None, content=None):
        try:
            if action == 'operate':
                self.help_page_settings('operate')
                self.driver.switch_to.frame('mainFrame')
                self.hpsh.help_page_element('help_page_edit')
                self.hpsh.help_page_element('help_page_return')
                self.hpsh.help_page_element('help_page_edit')
                self.hpsh.help_page_element('help_page_title_input', title=title)
                self.hpsh.help_page_element('help_page_information_type_options')
                self.hpsh.help_page_element('choice_beginner_institutional_services')
                self.hpsh.help_page_element('help_page_content_input', content=content)
                self.hpsh.help_page_element('help_page_preservation')
            else:
                return self.hpsh.help_page_element()
        except ValueError:
            print('操作‘编辑’功能失败！')

    # 操作‘添加帮助页’功能
    def add_help_page(self, action=None, title=None, content=None):
        try:
            if action == 'operate':
                self.help_page_settings('operate')
                self.driver.switch_to.frame('mainFrame')
                self.hpsh.help_page_element('add_help_page')
                self.hpsh.help_page_element('help_page_return')
                self.hpsh.help_page_element('add_help_page')
                self.hpsh.help_page_element('help_page_title_input', title=title)
                self.hpsh.help_page_element('help_page_information_type_options')
                self.hpsh.help_page_element('choice_beginner_institutional_services')
                self.hpsh.help_page_element('help_page_content_input', content=content)
                self.hpsh.help_page_element('help_page_preservation')
                self.hpsh.help_page_element('help_page_next_page')
            else:
                return self.hpsh.help_page_element('add_help_page_assert')
        except ValueError:
            print('操作‘添加帮助页’功能')
