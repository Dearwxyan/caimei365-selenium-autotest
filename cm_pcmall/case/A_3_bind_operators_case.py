# coding=utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.A_3_bind_operators_business import BindOperatorsBusiness
from time import sleep


class BindOperatorsCase(StartEnd):
    def test_A00_click_logo(self):
        '''
        检查点击登录页”logo“是否跳转【首页】
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.click_logo('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_00_login_click_logo.png')
        self.assertEqual(bob.click_logo(), '欢迎来到采美采购服务平台')

    def test_A01_click_login_btn(self):
        '''
        检查点击‘登录’链接是否跳转登录页面
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.click_login('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_01_click_login_btn.png')
        self.assertEqual(bob.click_login(), '欢迎登录\n生美 / 医美采购服务平台')

    def test_A02_account_number_null(self):
        '''
        检查账号为空，是否提示”请输入邮箱/手机号“
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.account_number_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_02_account_number_null.png')
        self.assertEqual(bob.account_number_null(), '邮箱/手机号不能为空')

    def test_A03_login_password_null(self):
        '''
        检查密码为空，是否提示”8-16位数字和字母组合不能为空“
        :return:
        '''

        bob = BindOperatorsBusiness(self.driver)
        bob.login_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_03_login_password_null.png')
        self.assertEqual(bob.login_password_null(), '8-16位数字和字母组合不能为空')

    def test_A04_full_name_null(self):
        '''
        检查姓名为空，是否提示”真实姓名不能为空“
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.full_name_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_04_full_name_null.png')
        self.assertEqual(bob.full_name_null(), '真实姓名不能为空')

    def test_A05_cell_phone_number_null(self):
        '''
        检查手机号为空，是否提示”您的常用手机号不能为空“
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.cell_phone_number_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_05_cell_phone_number_null.png')
        self.assertEqual(bob.cell_phone_number_null(), '您的常用手机号不能为空')

    def test_A06_sms_verification_code(self):
        '''
        检查验证码为空，是否提示”短信验证码不能为空“
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.sms_verification_code('action')
        sleep(2)
        om().screen_img(self.driver, 'A_3_06_sms_verification_code.png')
        self.assertEqual(bob.sms_verification_code(), '短信验证码不能为空')

    def test_A07_account_password_not_match(self):
        '''
        检查绑定运营原因，提示”参数异常：unionId不能为空！“
        :return:
        '''
        bob = BindOperatorsBusiness(self.driver)
        bob.account_password_not_match('action')
        sleep(2)
        om().screen_img('cm_pcmall', self.driver, 'A_3_07_account_password_not_match.png')
        self.assertEqual(bob.account_password_not_match(), '参数异常：unionId不能为空！')
