# coding = utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.C_1_not_login_index_page_business import NotLoginIndexBusiness
from time import sleep


class NotLoginIndexCase(StartEnd):
    def test_C01_click_login(self):
        '''
        检查点击‘登录’链接是否跳转登录页面
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.login_link('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_01_click_login.png')
        self.assertEqual(nlb.login_link(), '欢迎登录\n生美 / 医美采购服务平台')

    def test_C02_register_link(self):
        '''
        检查点击‘注册’链接是否跳转注册页面
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.register_link('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_02_register_link.png')
        self.assertEqual(nlb.register_link(), '请选择注册的账号类型')

    def test_C03_shopping_cart(self):
        '''
        检查点击‘购物车’链接是否跳转注册页面
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.shopping_cart('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_03_shopping_cart.png')
        self.assertEqual(nlb.shopping_cart(), '欢迎登录\n生美 / 医美采购服务平台')

    def test_C04_click_logo(self):
        '''
        检查点击‘logo’是否刷新首页
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.click_logo('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_04_click_logo.png')
        self.assertEqual(nlb.click_logo(), '欢迎来到采美采购服务平台')

    def test_C05_choose_products_search(self):
        '''
        检查搜索“产品”功能是否正常
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.choose_products_search('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_05_choose_products_search.png')
        self.assertEqual(nlb.choose_products_search('assert'), '搜索结果 > 卡琪紫红色精华油')
        self.assertEqual(nlb.choose_products_search(), '卡琪紫红色精华油')

    def test_C06_choose_supplier_search(self):
        '''
        检查搜索“供应商”功能是否正常
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.choose_supplier_search('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_06_choose_supplier_search.png')
        self.assertEqual(nlb.choose_supplier_search('assert'), '武汉深度科技有限公司')
        self.assertEqual(nlb.choose_supplier_search(), '所在地区：湖北省武汉市')

    def test_C07_choose_project_instrument_search(self):
        '''
        检查搜索“项目仪器”功能是否正常
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.choose_project_instrument_search('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_07_choose_project_instrument_search.png')
        self.assertEqual(nlb.choose_project_instrument_search(), '菲蜜丽私密青春激光')

    def test_C08_hot_search_words(self):
        '''
        检查“热搜词”搜索功能是否正常
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.hot_search_words('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_08_hot_search_words.png')
        self.assertEqual(nlb.hot_search_words(), '测试测试')

    def test_C09_official_account(self):
        '''
        检查点击“采美公众号”是否弹出关注二维码
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.official_account('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_09_official_account.png')
        self.assertEqual(nlb.official_account(), '采美公众号')

    def test_C10_caimei_small_program(self):
        '''
        检查点击“采美小程序”是否弹出关注二维码
        :return:
        '''
        nlb = NotLoginIndexBusiness(self.driver)
        nlb.caimei_small_program('action')
        sleep(2)
        om().screen_img(self.driver, 'C_1_10_caimei_small_program.png')
        self.assertEqual(nlb.caimei_small_program(), '采美小程序')