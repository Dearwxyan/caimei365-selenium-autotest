# coding=utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.B_2_club_register_business import ClubRegisterBusiness
from time import sleep
import unittest


class ClubRegisterCase(StartEnd):
    def test_B01_contacts_null(self):
        '''
        检查'联系人'为空，是否弹出提示‘联系人姓名不能为空’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.contacts_null('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_01_contacts_null.png')
        self.assertEqual(crb.contacts_null(), '联系人姓名不能为空')

    def test_B02_cell_phone_number_null(self):
        '''
        检查'手机号'为空，是否弹出提示‘您的常用手机号不能为空’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.cell_phone_number_null('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_02_cell_phone_number_null.png')
        self.assertEqual(crb.cell_phone_number_null(), '您的常用手机号不能为空')

    def test_B03_sms_verification_code_null(self):
        '''
        检查'短信验证码'为空，是否弹出提示‘短信验证码不能为空’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.sms_verification_code_null('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_03_sms_verification_code_null.png')
        self.assertEqual(crb.sms_verification_code_null(), '短信验证码不能为空')

    def test_B04_login_password_null(self):
        '''
        检查'登录密码'为空，是否弹出提示‘8-16位数字和字母组合不能为空’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.login_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_04_login_password_null.png')
        self.assertEqual(crb.login_password_null(), '8-16位数字和字母组合不能为空')

    def test_B05_confirm_password_null(self):
        '''
        检查'确认密码'为空，是否弹出提示‘不能为空’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.confirm_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_05_confirm_password_null.png')
        self.assertEqual(crb.confirm_password_null(), '不能为空')

    def test_B06_not_click_consent_permission(self):
        '''
        检查没有选择协议，是否弹出提示‘请阅读并同意协议’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.not_click_consent_permission('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_06_not_click_consent_permission.png')
        self.assertEqual(crb.not_click_consent_permission(), '请阅读并同意协议')

    def test_B07_cell_phone_number_error(self):
        '''
        检查手机号格式不正确，是否弹出提示‘手机号码格式不正确’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.cell_phone_number_error('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_07_cell_phone_number_error.png')
        self.assertEqual(crb.cell_phone_number_error(), '手机号码格式不正确')

    def test_B08_sms_verification_code_error(self):
        '''
        检查短信验证码不正确，是否弹出提示‘手机验证码错误’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.sms_verification_code_error('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_08_sms_verification_code_error.png')
        self.assertEqual(crb.sms_verification_code_error(), '手机验证码错误')

    def test_B09_password_is_inconsistent(self):
        '''
        检查密码不一致，是否弹出提示‘两次密码输入不一致’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.password_is_inconsistent('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_09_password_is_inconsistent.png')
        self.assertEqual(crb.password_is_inconsistent(), '两次密码输入不一致')

    def test_B10_register_success(self):
        '''
        检查成功注册(普通)机构，是否弹出提示‘注册成功！’
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.register_success('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_10_register_success.png')
        self.assertEqual(crb.register_success(), '注册成功！')

    def test_B11_upgrade_later_button(self):
        '''
        检查操作’先跳过，以后再升级‘是否跳转到普通机构个人中心页面
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.upgrade_later_button('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_11_upgrade_later_button.png')
        self.assertEqual(crb.upgrade_later_button(), '我的采美')

    def test_B12_confirm_upgrade_button(self):
        '''
        检查操作’确认升级并提交审核‘是否提交“会员机构”审核资料
        :return:
        '''
        crb = ClubRegisterBusiness(self.driver)
        crb.confirm_upgrade_button('action')
        sleep(2)
        om().screen_img(self.driver, 'B_2_12_confirm_upgrade_button.png')
        self.assertEqual(crb.confirm_upgrade_button(), '升级信息提交成功，审核通过后您的身份即成为会员机构并且获得500采美豆的奖励')