# coding = utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.D_1_order_process_business import OrderProcessBusiness
from time import sleep


class OrderProcessCase(StartEnd):
    def test_D01_add_to_cart(self):
        '''
        检查'机构登录'登录，进入商品详情，点击‘加入购物车’按钮是否将商品加入购物车
        :return:
        '''
        opb = OrderProcessBusiness(self.driver)
        opb.add_to_cart('action')
        om().screen_img(self.driver, 'D_1_01_add_to_cart.png')
        self.assertEqual(opb.add_to_cart('assert'), '商品已成功加入购物车！\n当前购物车共1种商品')

    def test_D02_to_settle_accounts(self):
        '''
        检查'机构登录'登录，进入商品详情，点击‘加入购物车’-“去结算“是否跳转进入“购物车”页面
        :return:
        '''
        opb = OrderProcessBusiness(self.driver)
        opb.to_settle_accounts('action')
        om().screen_img(self.driver, 'D_1_02_to_settle_accounts.png')
        self.assertEqual(opb.to_settle_accounts('assert'), '我的购物车')

    def test_D03_to_settle_accounts_btn(self):
        '''
        检查'机构登录'登录，进入商品详情，点击‘加入购物车’-“去结算“-"去结算"是否进入“提交订单”页面
        :return:
        '''
        opb = OrderProcessBusiness(self.driver)
        opb.to_settle_accounts_btn('action')
        om().screen_img(self.driver, 'D_1_03_to_settle_accounts_btn.png')
        self.assertEqual(opb.to_settle_accounts_btn('assert'), '确认订单(填写并核对订单信息)')

    def test_D04_place_order(self):
        '''
        检查'机构登录'登录，进入商品详情，点击‘加入购物车’-“去结算“-"去结算"-“提交订单”是否正常提交订单
        :return:
        '''
        opb = OrderProcessBusiness(self.driver)
        opb.place_order('action')
        om().screen_img(self.driver, 'D_1_04_place_order.png')
        self.assertEqual(opb.place_order('assert'), '收银台')

    def test_D05_buy_now(self):
        '''
        检查'机构登录'登录，进入商品详情，点击‘立即购买’按钮是否进去确认订单页面
        :return:
        '''
        opb = OrderProcessBusiness(self.driver)
        opb.buy_now('action')
        om().screen_img(self.driver, 'D_1_05_buy_now.png')
        self.assertEqual(opb.buy_now('assert'), '确认订单(填写并核对订单信息)')

    def test_D06_place_orders(self):
        '''
        检查'机构登录'登录，进入商品详情，点击‘立即购买’-“提交订单”是否正常提交订单
        :return:
        '''
        opb = OrderProcessBusiness(self.driver)
        opb.place_orders('action')
        om().screen_img(self.driver, 'D_1_06_place_orders.png')
        self.assertEqual(opb.place_orders('assert'), '收银台')

