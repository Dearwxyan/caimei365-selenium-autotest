 # coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
from time import sleep
import time
import random


class SupplierRegisterBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.num = str(random.randint(10000, 99999))
        self.data_name = 'pcmalldata.json'
        self.time = time.strftime("%Y%m%d", time.localtime())
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('supplier_register', 'url')
        self.config_name = self.oj.get_key_value('supplier_register', 'config_name')
        self.node_name = self.oj.get_key_value('supplier_register', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.cell_phone_number = self.oj.get_key_value('supplier_register', 'cell_phone_number')
        self.sms_verification_code = self.oj.get_key_value('supplier_register', 'sms_verification_code')
        self.login_password = self.oj.get_key_value('supplier_register', 'login_password')
        self.confirm_password = self.oj.get_key_value('supplier_register', 'confirm_password')
        self.corporate_name = self.oj.get_key_value('supplier_register', 'corporate_name')
        self.contacts = self.oj.get_key_value('supplier_register', 'contacts')
        self.mailbox = self.oj.get_key_value('supplier_register', 'mailbox')
        self.company_abbreviation = self.oj.get_key_value('supplier_register', 'company_abbreviation')
        self.detailed_address = self.oj.get_key_value('supplier_register', 'detailed_address')
        self.business_license_no = self.oj.get_key_value('supplier_register', 'business_license_no')
        self.business_license_picture = self.oj.get_key_value('supplier_register', 'business_license_picture')
        self.official_website_address = self.oj.get_key_value('supplier_register', 'official_website_address')
        self.wechat_official_account = self.oj.get_key_value('supplier_register', 'wechat_official_account')
        self.wechat_applet = self.oj.get_key_value('supplier_register', 'wechat_applet')
        self.product_description = self.oj.get_key_value('supplier_register', 'product_description')
        self.company_profile = self.oj.get_key_value('supplier_register', 'company_profile')

    # 点击“登录”链接
    def click_login(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'agree')
                self.po.operation_element('click', 'login_btn')
            else:
                return self.po.operation_element('assert', 'click_login_btn_assert')
        except IOError:
            print('Error:操作失败')

    # '注册'供应商提交审核
    def agree_submit_review(self, target=None):
        try:
            if target == 'action':
                self.clg.register_supplier(
                                      cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                      sms_verification_code=self.sms_verification_code,
                                      login_password=self.login_password,
                                      confirm_password=self.confirm_password,
                                      corporate_name=self.corporate_name,
                                      contacts=self.contacts,
                                      mailbox=self.num + ''.join(self.mailbox),
                                      company_abbreviation=''.join(self.company_abbreviation) + self.time,
                                      detailed_address=''.join(self.detailed_address) + self.num,
                                      business_license_no=''.join(self.business_license_no) + self.num,
                                      business_license_picture=self.business_license_picture,
                                      official_website_address=''.join(self.official_website_address),
                                      wechat_official_account=self.wechat_official_account,
                                      wechat_applet=self.wechat_applet,
                                      product_description=self.product_description,
                                      company_profile=self.company_profile)
            else:
                return self.po.operation_element('assert', 'agree_submit_review_assert')
        except IOError:
            print('Error:操作失败')

