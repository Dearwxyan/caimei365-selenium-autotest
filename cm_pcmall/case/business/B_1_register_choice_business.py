# coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness


class RegisterChoiceBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('register_choice', 'url')
        self.config_name = self.oj.get_key_value('register_choice', 'config_name')
        self.node_name = self.oj.get_key_value('register_choice', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)

    # 登录页点击“logo”
    def click_logo(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element(event='click', element_data='logo')
            else:
                return self.po.operation_element(event='assert', element_data='click_logo_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 点击”登录“链接跳转登录页面
    def click_login(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_btn')
            else:
                return self.po.operation_element('assert', 'click_login_btn_assert')
        except IOError:
            print("Error:操作失败")

    # 选择注册”机构“跳转注册机构页面
    def click_register_club(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'click_register_club')
            else:
                return self.po.operation_element('assert', 'click_register_club_assert')
        except IOError:
            print("Error:操作失败")

    # 选择注册”供应商“跳转注册机构页面
    def click_register_supplier(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'click_register_supplier')
            else:
                return self.po.operation_element('assert', 'click_register_supplier_assert')
        except IOError:
            print("Error:操作失败")




