# coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness


class ForgetPasswordBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('forget_password', 'url')
        self.config_name = self.oj.get_key_value('forget_password', 'config_name')
        self.node_name = self.oj.get_key_value('forget_password', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.cell_phone_number = self.oj.get_key_value('forget_password', 'cell_phone_number')
        self.sms_verification_code = self.oj.get_key_value('forget_password', 'sms_verification_code')
        self.new_password = self.oj.get_key_value('forget_password', 'new_password')
        self.confirm_password = self.oj.get_key_value('forget_password', 'confirm_password')
        self.cell_phone_number_errors = self.oj.get_key_value('forget_password', 'cell_phone_number_error')
        self.sms_verification_code_errors = self.oj.get_key_value('forget_password', 'ms_verification_code_error')
        self.password_not_conform_rule = self.oj.get_key_value('forget_password', 'password_not_conform_rules')
        self.password_is_inconsistents = self.oj.get_key_value('forget_password', 'password_is_inconsistent')
        self.supplier_change_password_successes = self.oj.get_key_value('forget_password',
                                                                        'supplier_change_password_success')
        self.mailbox = self.oj.get_key_value('forget_password', 'mailbox')
        self.email_verification_code = self.oj.get_key_value('forget_password', 'email_verification_code')
        self.email_login_password = self.oj.get_key_value('forget_password', 'email_login_password')
        self.email_confirms_password = self.oj.get_key_value('forget_password', 'email_confirms_password')
        self.mailbox_error = self.oj.get_key_value('forget_password', 'mailbox_error')
        self.email_verification_code_errors = self.oj.get_key_value('forget_password', 'email_verification_code_error')
        self.password_not_standards = self.oj.get_key_value('forget_password', 'password_not_standard')
        self.passwords_is_inconsistents = self.oj.get_key_value('forget_password', 'passwords_is_inconsistent')

    # 登录页点击“logo”
    def click_logo(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'logo')
            else:
                return self.po.operation_element('assert', 'click_logo_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 点击“登录”链接
    def click_login_btn(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_btn')
            else:
                return self.po.operation_element('assert', 'click_login_btn_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 手机号输入框为空，点击“提交”
    def cell_phone_number_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number='', sms_verification_code=self.sms_verification_code,
                                         new_password=self.new_password, confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'cell_phone_number_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 验证码输入框为空，点击“提交”
    def sms_verification_code_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number, sms_verification_code='',
                                         new_password=self.new_password, confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'sms_verification_code_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 新密码输入框为空，点击“提交”
    def new_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password='', confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'new_password_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 确认密码输入框为空，点击“提交”
    def confirm_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password=self.new_password, confirm_password='')
            else:
                return self.po.operation_element('assert', 'confirm_password_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 手机号格式不正确，点击“提交”
    def cell_phone_number_error(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number_errors,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password=self.new_password, confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'cell_phone_number_error_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 验证码不正确，点击“提交”
    def ms_verification_code_error(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number,
                                         sms_verification_code=self.sms_verification_code_errors,
                                         new_password=self.new_password, confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'ms_verification_code_error_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 密码不符合规则，点击“提交”
    def password_not_conform_rules(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password=self.password_not_conform_rule,
                                         confirm_password=self.password_not_conform_rule)
            else:
                return self.po.operation_element('assert', 'password_not_conform_rules_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 密码不一致，点击“提交”
    def password_is_inconsistent(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password=self.new_password,
                                         confirm_password=self.password_is_inconsistents)
            else:
                return self.po.operation_element('assert', 'password_is_inconsistent_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # [机构]正常修改密码，点击“提交”
    def club_change_password_success(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.cell_phone_number,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password=self.new_password,
                                         confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'change_password_success_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # [供应商]正常修改密码，点击“提交”
    def supplier_change_password_success(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(cell_phone_number=self.supplier_change_password_successes,
                                         sms_verification_code=self.sms_verification_code,
                                         new_password=self.new_password,
                                         confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'change_password_success_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，邮箱为空，点击“提交”
    def mailbox_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number='',
                                         sms_verification_code=self.email_verification_code,
                                         new_password=self.email_login_password,
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='mailbox_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，邮箱验证码为空，点击“提交”
    def email_verification_code_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code='',
                                         new_password=self.email_login_password,
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='email_verification_code_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，登录密码为空，点击“提交”
    def login_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code,
                                         new_password='',
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='login_password_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，确认密码为空，点击“提交”
    def confirms_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code,
                                         new_password=self.email_login_password,
                                         confirm_password='')
            else:
                return self.po.operation_element(event='assert', element_data='confirms_password_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，邮箱不正确，点击“提交”
    def mail_error(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox_error,
                                         sms_verification_code=self.email_verification_code,
                                         new_password=self.email_login_password,
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='mail_error_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，邮箱验证码不正确，点击“提交”
    def email_verification_code_error(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code_errors,
                                         new_password=self.email_login_password,
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='email_verification_code_error_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，密码不合规范，点击“提交”
    def password_not_standard(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code_errors,
                                         new_password=self.password_not_standards,
                                         confirm_password=self.password_not_standards)
            else:
                return self.po.operation_element(event='assert', element_data='password_not_standard_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，密码不一致，点击“提交”
    def passwords_is_inconsistent(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code_errors,
                                         new_password=self.email_login_password,
                                         confirm_password=self.passwords_is_inconsistents)
            else:
                return self.po.operation_element(event='assert', element_data='passwords_is_inconsistent_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，[机构]正常修改密码，点击“提交”
    def club_change_passwords_success(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code,
                                         new_password=self.email_login_password,
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='change_passwords_success_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 使用邮箱修改密码，[机构]正常修改密码，点击“提交”
    def supplier_change_passwords_success(self, target=None):
        try:
            if target == 'action':
                self.clg.change_password(type='mail', cell_phone_number=self.mailbox,
                                         sms_verification_code=self.email_verification_code,
                                         new_password=self.email_login_password,
                                         confirm_password=self.email_confirms_password)
            else:
                return self.po.operation_element(event='assert', element_data='change_passwords_success_assert')
        except IOError:
            print('Error:抱歉！操作失败')
