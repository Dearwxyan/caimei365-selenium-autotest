# coding=utf-8
import unittest
from cm_pcmall.case.business.common.start_browser import StartBrowser


# 启动、关闭浏览器
class StartEnd(unittest.TestCase):
    def setUp(self):
        self.sb = StartBrowser()
        # self.driver = self.sb.chrome_browser() # 无界面谷歌浏览器
        # self.driver = self.sb.firefox_browser()  # 无界面火狐浏览器
        self.driver = self.sb.debug_browser('firefox')  # 有界面浏览器
        self.driver.implicitly_wait(10)
        self.driver.maximize_window()

    def tearDown(self):
        self.driver.quit()
