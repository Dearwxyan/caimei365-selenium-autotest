# _*_ coding: utf-8 _*_
"""
Time:     2021/1/6 21:22
Author:   Wu Xiao Yan(Dearwxy)
Version:  V 0.1
File:     operation_json.py
"""
import os
import json


class OperationJson(object):
    def __init__(self, data_name):
        self.base_dir = os.path.dirname(os.path.abspath(__file__))
        self.base_dir = os.path.abspath(os.path.dirname(self.base_dir) + os.path.sep + ".")
        self.base_dir = self.base_dir.replace('\\', '/')
        self.json_path = self.base_dir + '/data/' + data_name
        self.data = self.read_data()

    # 读取json数据
    def read_data(self):
        with open(self.json_path, 'r', encoding='utf-8') as fp:
            data = json.load(fp)
            return data

    # 获取键值
    def get_key_value(self,keyword, key):
        data = self.data[keyword]
        return list(data[key])

if __name__ == '__main__':
    oj = OperationJson('admindata.json')
    print(oj.base_dir)
