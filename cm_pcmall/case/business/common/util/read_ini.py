# coding=utf-8
import os
import configparser


class ReadIni(object):
    def __init__(self,config_name,node_name):
        self.base_dir = os.path.dirname(os.path.abspath(__file__))
        self.base_dir = os.path.abspath(os.path.dirname(self.base_dir) + os.path.sep + ".")
        self.base_dir = self.base_dir.replace('\\', '/')
        self.base_dir = self.base_dir + '/config/' + ''.join(config_name)
        self.node = ''.join(node_name)
        self.cf = self.load_ini(self.base_dir)

    # 加载文件
    def load_ini(self, element_dir):
        cf = configparser.ConfigParser()
        cf.read(element_dir, encoding='UTF-8')
        return cf

    # 获取value值
    def get_value(self, key):
        data = self.cf.get(self.node, key)
        return data
