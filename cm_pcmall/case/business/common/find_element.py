# coding=utf-8
from cm_pcmall.case.business.common.util.read_ini import ReadIni


class FindElement(object):
    def __init__(self, driver, config_name, node_name):
        self.driver = driver
        self.config_name = config_name
        self.node_name = node_name
        self.ri = ReadIni(self.config_name, self.node_name)

    # 封装获取页面元素操作
    def get_element(self, key):
        data = self.ri.get_value(key)
        by = data.split('>')[0]
        value = data.split('>')[1]
        try:
            if by == 'id':
                return self.driver.find_element_by_id(value)                 # 通过“id”查找元素
            elif by == 'name':
                return self.driver.find_element_by_name(value)               # 通过“name”查找元素
            elif by == 'classname':
                return self.driver.find_element_by_class_name(value)         # 通过“class_name”查找元素
            elif by == 'tag_name':
                return self.driver.find_element_by_tag_name(value)           # 通过“tag_name”查找元素
            elif by == 'link_text':
                return self.driver.find_element_by_link_text(value)          # 通过“link_text”查找元素
            elif by == 'partial_link_text':
                return self.driver.find_element_by_partial_link_text(value)  # 通过“partial_link_text”查找元素
            elif by == 'xpath':
                return self.driver.find_element_by_xpath(value)              # 通过“xpath”查找元素
            elif by == 'css_selector':
                return self.driver.find_element_by_css_selector(value)       # 通过“css_selector”查找元素
        except IOError:
            print('Error：抱歉！没有该元素的定位方式')

