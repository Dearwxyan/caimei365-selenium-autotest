# coding=utf-8
from selenium.webdriver.common.keys import Keys
from cm_pcmall.case.business.common.page_element import PageElement


class PageOperation(object):
    def __init__(self, driver=None, url=None, config_name=None, node_name=None):
        self.driver = driver
        self.url = url
        self.config_name = config_name
        self.node_name = node_name
        self.pe = PageElement(self.driver, self.url, self.config_name, self.node_name)

    # 操作页面元素
    def operation_element(self, event, element_data, value=None):
        try:
            if event == 'input':
                self.pe.get_element('value', element_data).send_keys(value)
            elif event == 'click':
                self.pe.get_element('value', element_data).click()
            elif event == 'assert':
                return self.pe.get_element('value', element_data).text
        except IOError:
            print('Error:抱歉！页面元素操作失败')

    # a操作页面元素
    def operation_element_a(self, event, element_data, value=None):
        try:
            if event == 'input':
                self.pe.get_element('value', element_data).send_keys(Keys.CONTROL, 'a')
                self.pe.get_element('value', element_data).send_keys(value)
            elif event == 'click':
                self.pe.get_element('value', element_data).click()
            elif event == 'assert':
                return self.pe.get_element('value', element_data).text
        except IOError:
            print('Error:抱歉！页面元素操作失败')
