  # coding=utf-8
from cm_pcmall.case.business.common.find_element import FindElement


class PageElement(object):
    def __init__(self,driver=None, url=None, config_name=None, node_name=None):
        self.driver = driver
        self.url = url
        self.config_name = config_name
        self.node_name = node_name
        self.fe = FindElement(self.driver, self.config_name, self.node_name)

    # 打开测试网址
    def open(self):
        self.driver.get(''.join(self.url))

    # 获取页面元素
    def get_element(self, type, element_data):
        try:
            if type == 'value':
                return self.fe.get_element(element_data)
            elif type == 'assert':
                return self.fe.get_element(element_data)
        except IOError:
            print('Error:抱歉！找不到对应的页面元素')
