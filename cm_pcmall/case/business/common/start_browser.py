# coding=utf-8
import os
from selenium import webdriver

'''
1、设置浏览器无界面访问模式
2、无界面模式下默认不是全屏，所以需要设置一下分辨率
'''


class StartBrowser():
    def chrome_browser(self):
        # 启动谷歌无界面运行模式设置
        chrome_options = webdriver.ChromeOptions()
        chrome_options.set_headless()
        chrome_options.add_argument("–log-level=3")
        chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
        driver = webdriver.Chrome(chrome_options=chrome_options, service_log_path=os.devnull)
        driver.set_window_size(1920, 1080)
        return driver

    def firefox_browser(self):
        # 启动火狐无界面运行模式设置
        options = webdriver.FirefoxOptions()
        options.set_headless(True)
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        driver = webdriver.Firefox(options=options)
        driver.set_window_size(1920, 1080)
        return driver

    def debug_browser(self, browser):
        # 启动有界面浏览器调试模式
        if browser == 'chrome':
            driver = webdriver.Chrome()
        elif browser == 'firefox':
            driver = webdriver.Firefox()
        elif browser == 'phantomjs':
            driver = webdriver.PhantomJS()
        else:
            driver = webdriver.Ie()
        return driver
