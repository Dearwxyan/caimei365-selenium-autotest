 # coding=utf-8
import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.header import Header
from email.mime.base import MIMEBase
from email import encoders


class OperationMethod(object):
    def __init__(self):
        self.base_dir = os.path.dirname(os.path.abspath(__file__))
        self.base_dir = os.path.abspath(os.path.dirname(self.base_dir) + os.path.sep + ".")
        self.base_dir = self.base_dir.replace('\\', '/')

    # 截图保存
    def screen_img(self, driver, picture_name):
        try:
            picture_path = self.base_dir + '/screen_shot/' + picture_name
            driver.get_screenshot_as_file(picture_path)
        except BaseException as e:
            print("截图保存失败:" + str(e))

    # 获取最新报告路径
    def latest_report_path(self, item):
        try:
            report_path = self.base_dir + '/report/' + item + '/'
            report_lists = os.listdir(report_path)
            if len(report_lists) != 0:
                report_lists.sort(key=lambda fn: os.path.getatime(report_path + '\\' + fn))
                report_path = os.path.join(report_path, report_lists[-1])
                return report_path
        except BaseException as e:
            print("获取最新报告路径失败:" + str(e))


    # 读取报告内容
    def read_report(self, latest_report_path):
        try:
            if latest_report_path:
                open_report = open(latest_report_path, 'rb')
                report_content = open_report.read()
                report_content = MIMEText(report_content, 'html', 'utf-8')
                open_report.close()
                print('成功读取报告内容！')
                return report_content
        except BaseException as e:
            print("读取报告内容失败:" + str(e))

    # 添加报告附件
    def add_annex(self, latest_report_path, item=None):
        try:
            if latest_report_path:
                report_lists = os.path.split(latest_report_path)
                report_list = report_lists[-1]
                open_report = open(latest_report_path, 'rb')
                get_report = open_report.read()
                # if len(report_lists) == 0:
                #     print('文件夹中没有报告数据哦！！！')
                part = MIMEBase('application', 'octet-stream')
                part.set_payload(get_report)
                encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment', filename=('gbk', '', '%s' % report_list))
                open_report.close()
                print('成功添加附件！')
                return part
        except BaseException as e:
            print("添加报告附件失败:" + str(e))

    # 发送邮件及附件
    def send_mail(self, read_report, add_annex):
        if read_report:
            mail_server = 'smtp.qq.com'
            mail_server_password = 'ofzfylzqqtdbcjhd'
            mail_from = '3294428324@qq.com'
            receiver_data = ['1300162521@qq.com']  # 'zhjy_my520@foxmail.com'
            mail_receiver = ','.join(receiver_data)
            mail = MIMEMultipart()
            mail.attach(read_report)
            mail.attach(add_annex)
            mail['Subject'] = Header('采美商城自动化测试报告', 'utf-8')
            mail['From'] = mail_from
            mail['To'] = mail_receiver
            try:
                mail_server = smtplib.SMTP_SSL(mail_server, 465)
                mail_server.login(mail_from, mail_server_password)
                mail_server.helo(mail_server)
                mail_server.ehlo(mail_server)
                mail_server.sendmail(mail_from, mail_receiver.split(','), mail.as_string())
                print('成功发送邮件！')
            except BaseException as e:
                print("发送失败:" + str(e))
            finally:
                mail_server.quit()