# coding = utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
from time import sleep
import random


class ClubSignedInBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.num = str(random.randint(10000, 99999))
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('login', 'url')
        self.config_name = self.oj.get_key_value('club_signed', 'config_name')
        self.node_name = self.oj.get_key_value('club_signed', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.username = self.oj.get_key_value('club_signed', 'username')
        self.password = self.oj.get_key_value('club_signed', 'password')
        self.upload_avatars = self.oj.get_key_value('club_signed', 'upload_avatar')
        self.order_number_searchs = self.oj.get_key_value('club_signed', 'order_number_search')
        self.order_start_time = self.oj.get_key_value('club_signed', 'order_start_time')
        self.order_end_time = self.oj.get_key_value('club_signed', 'order_end_time')


    # 点击“我的采美”链接
    def select_my_beauty(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="select_my_beauty")
            elif target == 'assert1':
                return self.po.operation_element('assert', 'select_my_beauty_assert')
            elif target == 'assert2':
                return self.po.operation_element('assert', 'select_my_beauty_assert1')
            else:
                return self.po.operation_element('assert', 'select_my_beauty_assert2')
        except IOError:
            print('Error：操作失败')

    # 点击“机构资料”链接
    def select_club_information(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="club_information")
            else:
                return self.po.operation_element('assert', 'select_club_information_assert')
        except IOError:
            print('Error：操作失败')

    # 点击“退出登录”链接
    def select_log_out(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="log_out")
            else:
                return self.po.operation_element('assert', 'select_log_out_assert')
        except IOError:
            print('Error：操作失败')

    # 点击“购物车（空）”链接
    def shopping_cart(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="shopping_cart")
            else:
                return self.po.operation_element('assert', 'shopping_cart_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的采美”
    def click_menu_my_beauty(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="menu_my_beauty")
            else:
                return self.po.operation_element('assert', 'menu_my_beauty_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的采美”-》上传头像
    def upload_avatar(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="upload_avatar", upload_avatar=self.upload_avatars)
            else:
                return self.po.operation_element('assert', 'upload_avatar_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的采美”-》点击‘立即完善’
    def click_improve_immediately(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="improve_immediately")
            else:
                return self.po.operation_element('assert', 'improve_immediately_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’
    def click_my_order(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order")
            else:
                return self.po.operation_element('assert', 'my_order_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->点击'待确认'tab
    def click_to_be_confirmed(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order", order_type="to_be_confirmed")
            else:
                return self.po.operation_element('assert', 'click_to_be_confirmed_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->点击'待付款'tab
    def click_to_be_paid(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order", order_type="to_be_paid")
            else:
                return self.po.operation_element('assert', 'click_to_be_paid_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->点击'待发货'tab
    def click_to_be_delivered(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order", order_type="to_be_delivered")
            else:
                return self.po.operation_element('assert', 'click_to_be_delivered_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->点击'已发货'tab
    def click_delivered(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order", order_type="delivered")
            else:
                return self.po.operation_element('assert', 'click_delivered_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->点击'退货/款'tab
    def click_return_goods(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order", order_type="return_goods")
            else:
                return self.po.operation_element('assert', 'click_return_goods_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->点击'联系客服'
    def click_contact_customer_service(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order", order_type="contact_customer_service")
            else:
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'click_contact_customer_service_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"订单编号搜索"
    def order_number_search(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order",
                                     search_type="order_number_search",
                                     order_number_search=self.order_number_searchs)
            else:
                return self.po.operation_element('assert', 'order_number_search_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"下单时间搜索"
    def order_time(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order",
                                     search_type="order_time",
                                     order_start_time=self.order_start_time,
                                     order_end_time=self.order_end_time)
            else:
                return self.po.operation_element('assert', 'order_time_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"查看物流"
    def view_Logistics(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order",
                                     operate="view_Logistics")
            else:
                self.driver.switch_to.window(self.driver.window_handles[-1])
                return self.po.operation_element('assert', 'view_Logistics_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"订单详情"
    def order_details(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order",
                                     operate="view_Logistics")
            else:
                self.driver.switch_to.window(self.driver.window_handles[-1])
                return self.po.operation_element('assert', 'order_details_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"支付订单"
    def payment_order(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(self.username,self.password)
                self.clg.club_signed(type="my_order",
                                     operate="payment_order")
            else:
                self.driver.switch_to.window(self.driver.window_handles[-1])
                return self.po.operation_element('assert', 'payment_order_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"订单详情"->"查看物流"
    def views_Logistics(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'order_details')
                self.po.operation_element('click', 'views_Logistics')
            else:
                self.driver.switch_to.window(self.driver.window_handles[-1])
                return self.po.operation_element('assert', 'views_Logistics_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的订单’->"订单详情"->"支付订单"
    def payment_orders(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'order_details')
                self.po.operation_element('click', 'payment_orders')
            else:
                self.driver.switch_to.window(self.driver.window_handles[-1])
                return self.po.operation_element('assert', 'payment_orders_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’
    def click_my_maintenance(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                self.po.operation_element('click', 'my_maintenance')
            else:
                return self.po.operation_element('assert', 'my_maintenance_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’,操作’申请维修‘功能
    def apply_for_repair(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                self.po.operation_element('click', 'my_maintenance')
                sleep(3)
                self.po.operation_element('click', 'click_apply_for_repair')
                self.po.operation_element('input', 'contacts', '测试小吴')
                self.po.operation_element('input', 'contact_number', '159173'+ self.num)
                self.po.operation_element('click', 'province')
                self.po.operation_element('click', 'select_province')
                self.po.operation_element('click', 'city')
                self.po.operation_element('click', 'select_city')
                self.po.operation_element('click', 'area')
                self.po.operation_element('click', 'select_area')
                self.po.operation_element('input', 'detailed_address', '详细地址' + self.num)
                self.po.operation_element('input', 'instrument_name', '法诺智能美容')
                self.po.operation_element('input', 'manufacturer', '颜小琴')
                self.po.operation_element('input', 'upload_pictures', r'D:\autopic\logo.jpg')
                self.po.operation_element('input', 'upload_pictures1', r'D:\autopic\logo.jpg')
                self.po.operation_element('input', 'upload_pictures2', r'D:\autopic\logo.jpg')
                self.po.operation_element('input', 'upload_pictures3', r'D:\autopic\logo.jpg')
                self.po.operation_element('input', 'upload_pictures4', r'D:\autopic\logo.jpg')
                self.po.operation_element('input', 'problem_description', '仪器故障，申请维修！')
                self.po.operation_element('click', 'submit_button')

            else:
                return self.po.operation_element('assert', 'submit_button_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’->'关键词搜索'
    def key_word(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                sleep(3)
                self.po.operation_element('click', 'my_maintenance')
                self.po.operation_element('input', 'key_word', '二维热无若')
                self.po.operation_element('click', 'search_btn')
            else:
                return self.po.operation_element('assert', 'key_word_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’->'提交日期'
    def date_submission(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                sleep(3)
                self.po.operation_element('click', 'my_maintenance')
                self.po.operation_element('input', 'orders_start_time', '2021-04-19')
                self.po.operation_element('input', 'orders_end_time', '2021-04-21')
                self.po.operation_element('click', 'search_btn')
            else:
                return self.po.operation_element('assert', 'date_submission_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’->'维修进度'
    def maintenance_progress(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                sleep(4)
                self.po.operation_element('click', 'my_maintenance')
                self.po.operation_element('click', 'maintenance_progress')
                self.po.operation_element('click', 'maintenance_progress_select')
                self.po.operation_element('click', 'search_btn')
            else:
                return self.po.operation_element('assert', 'maintenance_progress_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’->点击'详情'
    def maintenance_details(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                sleep(3)
                self.po.operation_element('click', 'my_maintenance')
                self.po.operation_element('click', 'maintenance_details')
            else:
                return self.po.operation_element('assert', 'maintenance_details_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’->新增‘收货地址’
    def new_address(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                sleep(3)
                self.po.operation_element('click', 'receiving_address_management')
                self.po.operation_element('click', 'new_address')
                self.po.operation_element('input', 'consignee', '小吴')
                self.po.operation_element('input', 'contact_information', '159173' + self.num)
                self.po.operation_element('click', 'provinces')
                self.po.operation_element('click', 'select_provinces')
                self.po.operation_element('click', 'cities')
                self.po.operation_element('click', 'select_cities')
                self.po.operation_element('click', 'areas')
                self.po.operation_element('click', 'select_areas')
                self.po.operation_element('input', 'detailed_addresses', '测试地址' + self.num)
                self.po.operation_element('click', 'save_address_btn')
            else:
                return self.po.operation_element('assert', 'save_address_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“我的交易”-》点击‘我的维修’->删除‘收货地址’
    def confirm_deletion(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_my_deal')
                sleep(3)
                self.po.operation_element('click', 'receiving_address_management')
                self.po.operation_element('click', 'delete_address')
                self.po.operation_element('click', 'confirm_deletion_btn')
            else:
                return self.po.operation_element('assert', 'confirm_deletion_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘机构资料’
    def institutional_information(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'institutional_information')
            else:
                return self.po.operation_element('assert', 'institutional_information_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘机构资料’->修改机构资‘保存’
    def save_and_submit(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'institutional_information')
                self.po.operation_element('click', 'save_and_submit')
            else:
                return self.po.operation_element('assert', 'save_and_submit_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘运营人员管理’
    def operator_management(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'operator_management')
            else:
                return self.po.operation_element('assert', 'operator_management_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘运营人员管理’-'姓名'搜索
    def full_name_search(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'operator_management')
                self.po.operation_element('input', 'full_name', '小阿枫')
                self.po.operation_element('click', 'search_buttons')
            else:
                return self.po.operation_element('assert', 'full_name_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘运营人员管理’-'手机号'搜索
    def cell_phone_number_search(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'operator_management')
                self.po.operation_element('input', 'cell_phone_number', '15917360000')
                self.po.operation_element('click', 'search_buttons')
            else:
                return self.po.operation_element('assert', 'cell_phone_number_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘运营人员管理’-添加’运营人员‘
    def add_operators(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'operator_management')
                self.po.operation_element('click', 'add_operators')
                self.po.operation_element('input', 'full_names', '小吴')
                self.po.operation_element('input', 'cell_phone_numbers', '159173' + self.num)
                self.po.operation_element('click', 'ok_button')
            else:
                return self.po.operation_element('assert', 'add_operators_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“机构管理中心”-》点击‘运营人员管理’-删除’运营人员‘
    def delete_operators(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_organization_management_center')
                sleep(3)
                self.po.operation_element('click', 'operator_management')
                self.po.operation_element('click', 'delete_operators')
                self.po.operation_element('click', 'confirm_deletion')
            else:
                return self.po.operation_element('assert', 'delete_operators_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“账户设置”-》点击‘重置密码’
    def reset_password(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_account_settings')
                sleep(3)
                self.po.operation_element('click', 'reset_password')
            else:
                return self.po.operation_element('assert', 'reset_password_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“账户设置”-》点击‘重置密码’-》使用手机号修改密码
    def cell_phone_number_modify(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                # self.po.operation_element('input', 'username', '19999999998')
                # self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_account_settings')
                sleep(3)
                self.po.operation_element('click', 'reset_password')
                self.po.operation_element('input', 'cell_phone_number_input', '19999999998')
                self.po.operation_element('input', 'sms_acceptance_code_input', '666666')
                self.po.operation_element('input', 'new_password', '1111wwww')
                self.po.operation_element('input', 'confirm_password', '1111wwww')
                self.po.operation_element('click', 'submit_buttons')
            else:
                return self.po.operation_element('assert', 'modified_successfully_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“账户设置”-》点击‘重置密码’-》使用邮箱修改密码
    def mailbox_modify(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.clg.common_login(self.username, self.password)
                self.po.operation_element('input', 'username', '19999999998')
                self.po.operation_element('input', 'password', '1111wwww')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_account_settings')
                sleep(3)
                self.po.operation_element('click', 'reset_password')
                self.po.operation_element('click', 'switch_mailbox_modification')
                self.po.operation_element('input', 'mailbox', '23423432@125.com')
                self.po.operation_element('input', 'email_verification_code', '666666')
                self.po.operation_element('input', 'new_passwords', '1111aaaa')
                self.po.operation_element('input', 'confirm_passwords', '1111aaaa')
                self.po.operation_element('click', 'submits_buttons')
            else:
                return self.po.operation_element('assert', 'modified_successfully_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“账户设置”-》点击‘更换手机号’
    def change_mobile_phone_number(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '19999999998')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_account_settings')
                sleep(3)
                self.po.operation_element('click', 'change_mobile_phone_number')
            else:
                return self.po.operation_element('assert', 'change_mobile_phone_number_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“账户设置”-》点击‘更换手机号’->成功更换手机号成(19999999988)
    def successfully_replaced(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '19999999998')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_account_settings')
                sleep(3)
                self.po.operation_element('click', 'change_mobile_phone_number')
                self.po.operation_element('input', 'verification_code_original_phone_number', '666666')
                self.po.operation_element('input', 'new_mobile_phone_number', '19999999988')
                self.po.operation_element('input', 'verification_code_original_phone_number_new', '666666')
                self.po.operation_element('click', 'confirm_replacement_btn')
            else:
                return self.po.operation_element('assert', 'successfully_replaced_assert')
        except IOError:
            print('Error：操作失败')

    # 点击菜单“账户设置”-》点击‘更换手机号’->成功更换手机号(19999999998)
    def successfully_replaced_reduction(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '19999999988')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                # self.po.operation_element('click', 'nickname_options')
                # self.po.operation_element('click', 'select_my_beauty')
                self.po.operation_element('click', 'menu_account_settings')
                sleep(3)
                self.po.operation_element('click', 'change_mobile_phone_number')
                self.po.operation_element('input', 'verification_code_original_phone_number', '666666')
                self.po.operation_element('input', 'new_mobile_phone_number', '19999999998')
                self.po.operation_element('input', 'verification_code_original_phone_number_new', '666666')
                self.po.operation_element('click', 'confirm_replacement_btn')
            else:
                return self.po.operation_element('assert', 'successfully_replaced_assert')
        except IOError:
            print('Error：操作失败')