# coding = utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
import random
import datetime
from time import sleep


class ReleaseSecondHandBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.url = 'https://www-b.caimei365.com'
        self.config_name = 'PcmallElement.ini'
        self.node_name = 'ReleaseSecondHandElement'
        self.num = str(random.randint(10000, 99999))
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)

    # 首页，点击“二手市场介绍”按钮
    def secondhand_market_introduce(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'click_exit_Popup')
                self.po.operation_element('click', 'secondhand_market')
                self.driver.switch_to.window(self.driver.window_handles[1])
                self.driver.execute_script('window.scrollBy(0, 800)')
                self.po.operation_element('click', 'secondhand_market_introduce')
            else:
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'secondhand_market_introduce_assert')
        except IOError:
            print('Error：操作失败')

    # 首页，点击“发布二手”按钮
    def release_second_hand(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'click_exit_Popup')
                self.po.operation_element('click', 'secondhand_market')
                self.driver.switch_to.window(self.driver.window_handles[1])
                self.driver.execute_script('window.scrollBy(0, 800)')
                self.po.operation_element('click', 'release_second_hand')
                self.po.operation_element('click', 'classification_second_instruments')
                # self.po.operation_element('click', 'light_photo_electricity')
                # self.po.operation_element('click', 'heavy_photo_electricity')
                # self.po.operation_element('click', 'consumables')
                self.po.operation_element('click', 'commodity_brand')
                self.po.operation_element('click', 'commodity_brand_one')
                self.po.operation_element('input', 'trade_name', '自动化测试发布二手商品' + self.num)
                self.po.operation_element('input', 'date_of_production',  '2021年06月')
                self.po.operation_element('input', 'corporate_name', '自动化测试二手公司')
                self.po.operation_element('input', 'transaction_price', self.num)
                self.po.operation_element('input', 'commodity_quality', '九成新')
                self.po.operation_element('input', 'contacts', '吴小研')
                self.po.operation_element('input', 'contact_information', '145000' + self.num)
                self.po.operation_element('input', 'authentication_photos', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'pick_up_beauty', '吴小研')
                self.po.operation_element('input', 'pick_up_beauty_contact', '175000' + self.num)
                self.po.operation_element('click', 'commodity_type_medical_beauty')
                self.po.operation_element('click', 'province')
                self.po.operation_element('click', 'province_option')
                self.po.operation_element('click', 'city')
                self.po.operation_element('click', 'city_option')
                self.po.operation_element('click', 'area')
                self.po.operation_element('click', 'area_option')
                self.po.operation_element('input', 'detailed_address', '二手测试详细地址')
                self.po.operation_element('input', 'product_picture', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_two', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_three', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_four', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_five', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_details', '二手商品详细信息')
                self.po.operation_element('input', 'second_hand_transfer_commitment_picture', r'D:\autopic\5.jpg')
                # self.driver.execute_script('window.scrollBy(0, 2800)')
                self.po.operation_element('click', 'read')
                self.po.operation_element('click', 'release')
                # self.po.operation_element('click', 'continue_publishing')

            else:
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'release_assert')
        except IOError:
            print('Error：操作失败')

    # 首页，点击“发布二手”按钮
    def release_second_hands(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'click_exit_Popup')
                self.po.operation_element('click', 'secondhand_market')
                self.driver.switch_to.window(self.driver.window_handles[1])
                self.driver.execute_script('window.scrollBy(0, 800)')
                self.po.operation_element('click', 'release_second_hand')
                self.po.operation_element('click', 'classification_other')
                self.po.operation_element('click', 'commodity_brands')
                self.po.operation_element('click', 'commodity_brand_ones')
                self.po.operation_element('input', 'trade_name', '自动化测试发布二手商品' + self.num)
                self.po.operation_element('input', 'date_of_production',  '2021年06月')
                self.po.operation_element('input', 'corporate_name', '自动化测试二手公司')
                self.po.operation_element('input', 'transaction_price', self.num)
                self.po.operation_element('click', 'price_details')
                self.po.operation_element('input', 'commodity_quality', '九成新')
                self.po.operation_element('input', 'contacts', '吴小研')
                self.po.operation_element('input', 'contact_information', '145000' + self.num)
                self.po.operation_element('input', 'authentication_photos', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'pick_up_beautys', '吴小研')
                self.po.operation_element('input', 'pick_up_beauty_contacts', '175000' + self.num)
                self.po.operation_element('click', 'commodity_type_no_medical_beautys')
                self.po.operation_element('click', 'province')
                self.po.operation_element('click', 'province_option')
                self.po.operation_element('click', 'city')
                self.po.operation_element('click', 'city_option')
                self.po.operation_element('click', 'area')
                self.po.operation_element('click', 'area_option')
                self.po.operation_element('input', 'detailed_address', '二手测试详细地址')
                self.po.operation_element('input', 'product_picture', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_two', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_three', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_four', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_five', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_details', '二手商品详细信息')
                self.po.operation_element('input', 'second_hand_transfer_commitment_picture', r'D:\autopic\5.jpg')
                self.po.operation_element('click', 'reads')
                self.po.operation_element('click', 'release')
                self.po.operation_element('click', 'continue_publishing')
            else:
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'release_assert')
        except IOError:
            print('Error：操作失败')
