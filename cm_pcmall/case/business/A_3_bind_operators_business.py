# coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
import random


class BindOperatorsBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.num = str(random.randint(1000, 9999))
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('bind_operators', 'url')
        self.config_name = self.oj.get_key_value('bind_operators', 'config_name')
        self.node_name = self.oj.get_key_value('bind_operators', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.account_numbers = self.oj.get_key_value('bind_operators', 'account_number')
        self.login_passwords = self.oj.get_key_value('bind_operators', 'login_password')
        self.full_names = self.oj.get_key_value('bind_operators', 'full_name')
        self.cell_phone_numbers = self.oj.get_key_value('bind_operators', 'cell_phone_number')
        self.sms_verification_codes = self.oj.get_key_value('bind_operators', 'sms_verification_code')
        self.account_password_not_matchs = self.oj.get_key_value('bind_operators', 'account_password_not_match')

    # 登录页点击“logo”
    def click_logo(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element(event='click', element_data='logo')
            else:
                return self.po.operation_element(event='assert', element_data='click_logo_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 点击“登录”链接
    def click_login(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_btn')
            else:
                return self.po.operation_element('assert', 'click_login_btn_assert')
        except IOError:
            print('Error:操作失败')

    # 账号为空，提示信息
    def account_number_null(self, target=None):
        try:
            if target == 'action':
                self.clg.bind_operators(account_number='',
                                        login_password=self.login_passwords,
                                        full_name=self.full_names,
                                        cell_phone_number=''.join(self.cell_phone_numbers) + self.num,
                                        sms_verification_code=self.sms_verification_codes)
            else:
                return self.po.operation_element('assert', 'account_number_null_assert')
        except IOError:
            print('Error:操作失败')

    # 密码为空，提示信息
    def login_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.bind_operators(account_number=self.account_numbers,
                                        login_password='',
                                        full_name=self.full_names,
                                        cell_phone_number=''.join(self.cell_phone_numbers) + self.num,
                                        sms_verification_code=self.sms_verification_codes)
            else:
                return self.po.operation_element('assert', 'login_password_null_assert')
        except IOError:
            print('Error:操作失败')

    # 姓名为空，提示信息
    def full_name_null(self, target=None):
        try:
            if target == 'action':
                self.clg.bind_operators(account_number=self.account_numbers,
                                        login_password=self.login_passwords,
                                        full_name='',
                                        cell_phone_number=''.join(self.cell_phone_numbers) + self.num,
                                        sms_verification_code=self.sms_verification_codes)
            else:
                return self.po.operation_element('assert', 'full_name_null_assert')
        except IOError:
            print('Error:操作失败')

    # 手机号为空，提示信息
    def cell_phone_number_null(self, target=None):
        try:
            if target == 'action':
                self.clg.bind_operators(account_number=self.account_numbers,
                                        login_password=self.login_passwords,
                                        full_name=self.full_names,
                                        cell_phone_number='',
                                        sms_verification_code=self.sms_verification_codes)
            else:
                return self.po.operation_element('assert', 'cell_phone_number_null_assert')
        except IOError:
            print('Error:操作失败')

    # 短信验证码为空，提示信息
    def sms_verification_code(self, target=None):
        try:
            if target == 'action':
                self.clg.bind_operators(account_number=self.account_numbers,
                                        login_password=self.login_passwords,
                                        full_name=self.full_names,
                                        cell_phone_number=''.join(self.cell_phone_numbers) + self.num,
                                        sms_verification_code='')
            else:
                return self.po.operation_element('assert', 'sms_verification_code_assert')
        except IOError:
            print('Error:操作失败')

    # 账号密码不匹配，提示信息
    def account_password_not_match(self, target=None):
        try:
            if target == 'action':
                self.clg.bind_operators(account_number=self.account_numbers,
                                        login_password=self.account_password_not_matchs,
                                        full_name=self.full_names,
                                        cell_phone_number=''.join(self.cell_phone_numbers) + self.num,
                                        sms_verification_code=self.sms_verification_codes)
            else:
                return self.po.operation_element('assert', 'account_password_not_match_assert')
        except IOError:
            print('Error:操作失败')
