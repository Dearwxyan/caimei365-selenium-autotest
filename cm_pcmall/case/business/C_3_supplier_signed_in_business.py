# coding = utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
from selenium.webdriver.common.action_chains import ActionChains
import random
import time
from time import sleep


class SupplierSignedInBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.url = 'https://www-b.caimei365.com/login.html'
        self.config_name = 'PcmallElement.ini'
        self.node_name = 'SupplierSignedInElement'
        self.num = str(random.randint(10000, 99999))
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)

    # 点击“我的采美”链接
    def select_my_beauty(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'nickname_options')
                self.po.operation_element('click', 'select_my_beauty')
            elif target == 'assert1':
                return self.po.operation_element('assert', 'select_my_beauty_assert')
            elif target == 'assert2':
                return self.po.operation_element('assert', 'select_my_beauty_assert1')
            elif target == 'assert3':
                return self.po.operation_element('assert', 'select_my_beauty_assert2')
            else:
                return self.po.operation_element('assert', 'select_my_beauty_assert3')
        except IOError:
            print('Error：操作失败')

    # 点击“店铺管理”链接
    def select_store_management(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'nickname_options')
                self.po.operation_element('click', 'select_store_management')
            elif target == 'assert1':
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'select_store_management_assert')
            else:
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'select_store_management_assert1')
        except IOError:
            print('Error：操作失败')

    # 点击“退出登录”链接
    def select_log_out(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'nickname_options')
                self.po.operation_element('click', 'select_log_out')
            else:
                return self.po.operation_element('assert', 'select_log_out_assert')
        except IOError:
            print('Error：操作失败')

    # 操作“上传头像”功能
    def upload_avatar(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_beauty')
                self.po.operation_element('input', 'upload_avatar', r'D:\autopic\logo.jpg')
        except IOError:
            print('Error：操作失败')

    # 操作“完善资料”功能
    def improve_immediately(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element_a('input', 'username', '18666666666')
                self.po.operation_element_a('input', 'password', '1111aaaa')
                self.po.operation_element_a('click', 'login_btn')
                self.po.operation_element_a('click', 'my_beauty')
                self.po.operation_element_a('click', 'improve_immediately')
                self.po.operation_element_a('input', 'fixed_telephone', '888' + self.num)
                self.po.operation_element_a('input', 'fax', '26' + self.num)
                self.po.operation_element_a('input', 'corporate_representative', '吴小帅')
                self.po.operation_element_a('input', 'registered_capital', '9999')
                self.po.operation_element_a('input', 'nature_company', '美业电商')
                self.po.operation_element_a('input', 'annual_turnover', '8888')
                self.po.operation_element_a('click', 'submit')
            else:
                return self.po.operation_element('assert', 'improve_information_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“订单编号”查询功能
    def order_number_query(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                sleep(2)
                self.po.operation_element('input', 'order_number', 'H16209773262340301')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'order_number_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“买家名称”查询功能
    def buyer_name_query(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('input', 'buyer_name', '小傻瓜')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'buyer_name_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“结算状态”-“待结算”查询功能
    def delivery_status_one(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'delivery_status')
                self.po.operation_element('click', 'delivery_status_one')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'delivery_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“结算状态”-“部分结算”查询功能
    def delivery_status_two(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'delivery_status')
                self.po.operation_element('click', 'delivery_status_two')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'delivery_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“结算状态”-“已结算”查询功能
    def delivery_status_three(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'delivery_status')
                self.po.operation_element('click', 'delivery_status_three')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'delivery_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“发货状态”-“待发货”查询功能
    def settlement_status_one(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'settlement_status')
                self.po.operation_element('click', 'settlement_status_one')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'settlement_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“发货状态”-“部分发货”查询功能
    def settlement_status_two(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'settlement_status')
                self.po.operation_element('click', 'settlement_status_two')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'settlement_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》我的订单，操作“发货状态”-“已发货”查询功能
    def settlement_status_three(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'my_order')
                self.po.operation_element('click', 'settlement_status')
                self.po.operation_element('click', 'settlement_status_three')
                self.po.operation_element('click', 'query')
            else:
                return self.po.operation_element('assert', 'settlement_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》结算管理，操作“订单编号”查询功能
    def settlement_order_number(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'settlement_management')
                self.po.operation_element('input', 'settlement_order_number', 'X16221881460891701')
                self.po.operation_element('click', 'settlement_query')
            else:
                return self.po.operation_element('assert', 'settlement_order_number_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》结算管理，操作“买家名称”查询功能
    def settlement_buyer_name(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'settlement_management')
                self.po.operation_element('input', 'settlement_buyer_name', 'OMG')
                self.po.operation_element('click', 'settlement_query')
            else:
                return self.po.operation_element('assert', 'settlement_buyer_name_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》结算管理，操作“结算状态”-“待结算”查询功能
    def settlement_delivery_status_one(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'settlement_management')
                self.po.operation_element('click', 'settlement_delivery_status')
                self.po.operation_element('click', 'settlement_delivery_status_one')
                self.po.operation_element('click', 'settlement_query')
            else:
                return self.po.operation_element('assert', 'settlement_delivery_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》结算管理，操作“结算状态”-“待结算”查询功能
    def settlement_delivery_status_two(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'settlement_management')
                self.po.operation_element('click', 'settlement_delivery_status')
                self.po.operation_element('click', 'settlement_delivery_status_two')
                self.po.operation_element('click', 'settlement_query')
            else:
                return self.po.operation_element('assert', 'settlement_delivery_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的交易-》结算管理，操作“结算状态”-“待结算”查询功能
    def settlement_delivery_status_three(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_deal')
                self.po.operation_element('click', 'settlement_management')
                self.po.operation_element('click', 'settlement_delivery_status')
                self.po.operation_element('click', 'settlement_delivery_status_three')
                self.po.operation_element('click', 'settlement_query')
            else:
                return self.po.operation_element('assert', 'settlement_delivery_status_assert')
        except IOError:
            print('Error：操作失败')

    # 我的店铺-》查看店铺，操作“查看店铺”功能
    def view_store(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_shop')
                self.po.operation_element('click', 'view_store')
            else:
                self.driver.switch_to.window(self.driver.window_handles[0])
                return self.po.operation_element('assert', 'view_store_assert')
        except IOError:
            print('Error：操作失败')

    # 我的店铺-》查看店铺，操作“装扮主页”功能
    def dress_up_home_page(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_shop')
                self.po.operation_element('click', 'dress_up_home_page')
            else:
                self.driver.switch_to.window(self.driver.window_handles[0])
                return self.po.operation_element('assert', 'dress_up_home_page_assert')
        except IOError:
            print('Error：操作失败')

    # 我的店铺-》发布商品，操作“发布商品”功能
    def release_product(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18812345678')
                self.po.operation_element('input', 'password', '1111aaaa')
                ActionChains(self.driver).drag_and_drop_by_offset(self.po.pe.get_element('slider'),self.po.pe.get_element('slider_area').size['width'],self.po.pe.get_element('slider').size['height']).perform()
                sleep(3)
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_shop')
                self.po.operation_element('click', 'release_product')
                self.po.operation_element('input', 'product_display_name', time.asctime( time.localtime(time.time()) ) + '法国CRISTAL绿塑冷冻溶脂' + self.num)
                self.po.operation_element('input', 'internal_trade_name', '法国CRISTAL绿塑冷冻溶脂' + self.num)
                self.po.operation_element('click', 'commodity_attributes')
                self.po.operation_element('click', 'commodity_classification')
                self.po.operation_element('click', 'primary_classification_one')
                self.po.operation_element('click', 'secondary_classification')
                self.po.operation_element('click', 'secondary_classification_one')
                self.po.operation_element('click', 'three_level_classification')
                self.po.operation_element('click', 'three_level_classification_one')
                self.po.operation_element('input', 'commodity_brand', 'gk' + self.num )
                self.po.operation_element('click', 'testing')
                self.po.operation_element('click', 'to_submit')
                self.po.operation_element('click', 'determine')
                self.po.operation_element('click', 'determine_two')
                # sleep(3)
                self.po.operation_element('click', 'commodity_type')
                self.po.operation_element('click', 'commodity_type_one')
                self.po.operation_element('input', 'product_label', '面部紧致提升去皱')
                self.po.operation_element('click', 'add_tags_btn')
                self.po.operation_element('input', 'package_specification', '台')
                self.po.operation_element('input', 'search_keywords_one', 'CRISTAL')
                self.po.operation_element('input', 'search_keywords_two', '法国CRISTAL')
                self.po.operation_element('input', 'search_keywords_three', '绿塑')
                self.po.operation_element('input', 'search_keywords_four', '冷冻溶脂')
                self.po.operation_element('input', 'search_keywords_five', '绿塑冷冻溶脂')
                self.po.operation_element('input', 'related_parameters_name', '屏幕尺寸')
                self.po.operation_element('input', 'related_parameters_information', '22-24.5英寸')
                self.po.operation_element('click', 'add_parameters_btn')
                self.po.operation_element('input', 'related_parameters_name_two', '商品产地')
                self.po.operation_element('input', 'related_parameters_information_two', '中国大陆')
                self.po.operation_element('click', 'next_step_one')
                # sleep(2)
                self.po.operation_element('input', 'market_value', self.num)
                self.po.operation_element('input', 'price', self.num)
                self.po.operation_element('click', 'tax_included')
                self.po.operation_element('click', 'special_ticket')
                self.po.operation_element('input', 'settlement_price', self.num)
                self.po.operation_element('input', 'minimum_order_quantity', '2')
                self.po.operation_element('input', 'stock', self.num)
                # sleep(2)
                self.po.operation_element('click', 'next_step_two')
                self.po.operation_element('input', 'product_picture', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_two', r'D:\autopic\5.png')
                self.po.operation_element('input', 'product_picture_three', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_four', r'D:\autopic\5.png')
                self.po.operation_element('input', 'product_picture_five', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_details', 'Fotona 4D激光平台是全球首创用于无创眼部精雕祛眼袋&微笑法令纹提拉的激光系统 ，汇聚4种专利的Smooth Er, Frac3、Piano和Superficial技术于一身，独创全球唯一“内外联合、分层治疗”的4D治疗手法，无创、无痛、无间工期，安全可靠为您抹去岁月痕迹，是无创祛眼袋和祛除法令纹治疗的首选')
                # sleep(2)
                self.po.operation_element('click', 'next_step_three')
                self.po.operation_element('input', 'ordering_scheme', 'Fotona 4D激光平台是全球首创用于无创眼部精雕祛眼袋&微笑法令纹提拉的激光系统 ，汇聚4种专利的Smooth Er, Frac3、Piano和Superficial技术于一身，独创全球唯一“内外联合、分层治疗”的4D治疗手法，无创、无痛、无间工期，安全可靠为您抹去岁月痕迹，是无创祛眼袋和祛除法令纹治疗的首选')
                self.po.operation_element('input', 'service_details', 'Fotona 4D激光平台是全球首创用于无创眼部精雕祛眼袋&微笑法令纹提拉的激光系统 ，汇聚4种专利的Smooth Er, Frac3、Piano和Superficial技术于一身，独创全球唯一“内外联合、分层治疗”的4D治疗手法，无创、无痛、无间工期，安全可靠为您抹去岁月痕迹，是无创祛眼袋和祛除法令纹治疗的首选')
                self.po.operation_element('click', 'submit_for_review')
            else:
                return self.po.operation_element('assert', 'submit_for_review_assert')
        except IOError:
            print('Error：操作失败')

    # 我的店铺-》发布商品，操作“发布商品”功能
    def preview_product(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_shop')
                self.po.operation_element('click', 'release_product')
                self.po.operation_element('input', 'product_display_name', '法国CRISTAL绿塑冷冻溶脂' + self.num)
                self.po.operation_element('input', 'internal_trade_name', '法国CRISTAL绿塑冷冻溶脂' + self.num)
                self.po.operation_element('click', 'commodity_attributes')
                self.po.operation_element('click', 'commodity_classification')
                self.po.operation_element('click', 'primary_classification_one')
                self.po.operation_element('click', 'secondary_classification')
                self.po.operation_element('click', 'secondary_classification_one')
                self.po.operation_element('click', 'three_level_classification')
                self.po.operation_element('click', 'three_level_classification_one')
                self.po.operation_element('input', 'commodity_brand', 'gk' + self.num )
                self.po.operation_element('click', 'testing')
                self.po.operation_element('click', 'to_submit')
                self.po.operation_element('click', 'determine')
                self.po.operation_element('click', 'determine_two')
                # sleep(3)
                self.po.operation_element('click', 'commodity_type')
                self.po.operation_element('click', 'commodity_type_one')
                self.po.operation_element('input', 'product_label', '面部紧致提升去皱')
                self.po.operation_element('click', 'add_tags_btn')
                self.po.operation_element('input', 'package_specification', '台')
                self.po.operation_element('input', 'search_keywords_one', 'CRISTAL')
                self.po.operation_element('input', 'search_keywords_two', '法国CRISTAL')
                self.po.operation_element('input', 'search_keywords_three', '绿塑')
                self.po.operation_element('input', 'search_keywords_four', '冷冻溶脂')
                self.po.operation_element('input', 'search_keywords_five', '绿塑冷冻溶脂')
                self.po.operation_element('input', 'related_parameters_name', '屏幕尺寸')
                self.po.operation_element('input', 'related_parameters_information', '22-24.5英寸')
                self.po.operation_element('click', 'add_parameters_btn')
                self.po.operation_element('input', 'related_parameters_name_two', '商品产地')
                self.po.operation_element('input', 'related_parameters_information_two', '中国大陆')
                self.po.operation_element('click', 'next_step_one')
                # sleep(2)
                self.po.operation_element('input', 'market_value', self.num)
                self.po.operation_element('input', 'price', self.num)
                self.po.operation_element('click', 'tax_included')
                self.po.operation_element('click', 'special_ticket')
                self.po.operation_element('input', 'settlement_price', self.num)
                self.po.operation_element('input', 'minimum_order_quantity', '2')
                self.po.operation_element('input', 'stock', self.num)
                self.po.operation_element('click', 'next_step_two')
                # sleep(2)
                self.po.operation_element('input', 'product_picture', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_two', r'D:\autopic\5.png')
                self.po.operation_element('input', 'product_picture_three', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_picture_four', r'D:\autopic\5.png')
                self.po.operation_element('input', 'product_picture_five', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'product_details', 'Fotona 4D激光平台是全球首创用于无创眼部精雕祛眼袋&微笑法令纹提拉的激光系统 ，汇聚4种专利的Smooth Er, Frac3、Piano和Superficial技术于一身，独创全球唯一“内外联合、分层治疗”的4D治疗手法，无创、无痛、无间工期，安全可靠为您抹去岁月痕迹，是无创祛眼袋和祛除法令纹治疗的首选')
                self.po.operation_element('click', 'next_step_three')
                # sleep(2)
                self.po.operation_element('input', 'ordering_scheme', 'Fotona 4D激光平台是全球首创用于无创眼部精雕祛眼袋&微笑法令纹提拉的激光系统 ，汇聚4种专利的Smooth Er, Frac3、Piano和Superficial技术于一身，独创全球唯一“内外联合、分层治疗”的4D治疗手法，无创、无痛、无间工期，安全可靠为您抹去岁月痕迹，是无创祛眼袋和祛除法令纹治疗的首选')
                self.po.operation_element('input', 'service_details', 'Fotona 4D激光平台是全球首创用于无创眼部精雕祛眼袋&微笑法令纹提拉的激光系统 ，汇聚4种专利的Smooth Er, Frac3、Piano和Superficial技术于一身，独创全球唯一“内外联合、分层治疗”的4D治疗手法，无创、无痛、无间工期，安全可靠为您抹去岁月痕迹，是无创祛眼袋和祛除法令纹治疗的首选')
                self.po.operation_element('click', 'preview_product')
            else:
                self.driver.switch_to.window(self.driver.window_handles[1])
                return self.po.operation_element('assert', 'preview_product_assert')
        except IOError:
            print('Error：操作失败')

    # 我的店铺-》我的商品，操作“我的商品”功能
    def my_product(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_shop')
                self.po.operation_element('click', 'my_product')
            else:
                return self.po.operation_element('assert', 'my_product_assert')
        except IOError:
            print('Error：操作失败')

    # 我的店铺-》品牌管理，操作“提交新品牌”功能
    def submit_brand(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'my_shop')
                self.po.operation_element('click', 'brand_management')
                self.po.operation_element('click', 'submit_new_brand')
                self.po.operation_element('input', 'brand_name', '新品牌' + self.num )
                self.po.operation_element('input', 'brand_logo', r'D:\autopic\5.jpg')
                self.po.operation_element('input', 'brand_description', '这是新品牌描述' + self.num)
                self.po.operation_element('click', 'submit_brand')
            else:
                return self.po.operation_element('assert', 'submit_brand_assert')
        except IOError:
            print('Error：操作失败')

    # 管理中心-》资料信息，操作“提交资料”功能
    def submit_information(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'management_center')
                self.po.operation_element('click', 'information')
                self.po.operation_element_a('input', 'annual_turnovers', self.num)
                self.po.operation_element('click', 'submit_information')
            else:
                return self.po.operation_element('assert', 'submit_information_assert')
        except IOError:
            print('Error：操作失败')

    # 管理中心-》员工管理，操作“添加运营人员”功能
    def staff_management(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'management_center')
                self.po.operation_element('click', 'staff_management')
                self.po.operation_element('click', 'add_operators')
                self.po.operation_element_a('input', 'operators_name', '小吴')
                self.po.operation_element_a('input', 'operators_phone_number', '123456'+ self.num)
                self.po.operation_element('click', 'determines')
            else:
                return self.po.operation_element('assert', 'determines_assert')
        except IOError:
            print('Error：操作失败')

    # 账户设置-》重置密码，操作“手机号修改密码”功能
    def reset_password(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'account_setup')
                self.po.operation_element('click', 'reset_password')
                self.po.operation_element('input', 'cell_phone_number', '18666666666')
                self.po.operation_element('input', 'sms_verification_code', '666666')
                self.po.operation_element('input', 'new_password', '1111wwww')
                self.po.operation_element('input', 'confirm_password', '1111wwww')
                self.po.operation_element('click', 'submit_password')
            else:
                return self.po.operation_element('assert', 'submit_password_assert')
        except IOError:
            print('Error：操作失败')

    # 账户设置-》重置密码，操作“邮箱修改密码”功能
    def switch_mailbox(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111wwww')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'account_setup')
                self.po.operation_element('click', 'reset_password')
                self.po.operation_element('click', 'switch_mailbox')
                self.po.operation_element('input', 'mailbox', '18666666666@qq.com')
                self.po.operation_element('input', 'email_verification_code', '666666')
                self.po.operation_element('input', 'login_password', '1111aaaa')
                self.po.operation_element('input', 'confirm_passwords', '1111aaaa')
                self.po.operation_element('click', 'submit_password')
            else:
                return self.po.operation_element('assert', 'submit_password_assert')
        except IOError:
            print('Error：操作失败')

    # 账户设置-》更换手机，操作“更换手机”功能
    def change_mobile_phone_number(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18666666666')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'account_setup')
                self.po.operation_element('click', 'change_mobile_phone_number')
                self.po.operation_element('input', 'original_mobile_phone_code', '666666')
                self.po.operation_element('input', 'new_mobile_phone_number', '18777777776')
                self.po.operation_element('input', 'new_mobile_phone_code', '666666')
                self.po.operation_element('click', 'confirm_replacement')
            else:
                return self.po.operation_element('assert', 'confirm_replacement_assert')
        except IOError:
            print('Error：操作失败')

    # 账户设置-》更换手机，操作“更换手机”功能
    def change_mobile_phone_numbers(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('input', 'username', '18777777776')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'account_setup')
                self.po.operation_element('click', 'change_mobile_phone_number')
                self.po.operation_element('input', 'original_mobile_phone_code', '666666')
                self.po.operation_element('input', 'new_mobile_phone_number', '18666666666')
                self.po.operation_element('input', 'new_mobile_phone_code', '666666')
                self.po.operation_element('click', 'confirm_replacement')
            else:
                return self.po.operation_element('assert', 'confirm_replacement_assert')
        except IOError:
            print('Error：操作失败')