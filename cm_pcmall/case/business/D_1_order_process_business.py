# coding = utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
import random
from time import sleep


class OrderProcessBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.url = 'https://www-b.caimei365.com/product-6415.html'
        self.config_name = 'PcmallElement.ini'
        self.node_name = 'OrderProcessElement'
        self.num = str(random.randint(10000, 99999))
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)

    # 商品详情，点击“加入购物车”按钮
    def add_to_cart(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_to_view_price')
                self.po.operation_element('input', 'username', '12300087024')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'add_to_cart')
            else:
                return self.po.operation_element('assert', 'add_to_cart_assert')
        except IOError:
            print('Error：操作失败')

    # 商品详情，点击“加入购物车”-“去结算”
    def to_settle_accounts(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_to_view_price')
                self.po.operation_element('input', 'username', '12300087024')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'add_to_cart')
                self.po.operation_element('click', 'to_settle_accounts')
            else:
                self.driver.switch_to.window(self.driver.window_handles[0])
                return self.po.operation_element('assert', 'to_settle_accounts_assert')
        except IOError:
            print('Error：操作失败')

    # 商品详情，点击“加入购物车”-“去结算”-“去结算”
    def to_settle_accounts_btn(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_to_view_price')
                self.po.operation_element('input', 'username', '12300087024')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'add_to_cart')
                self.po.operation_element('click', 'to_settle_accounts')
                self.driver.switch_to.window(self.driver.window_handles[0])
                self.po.operation_element('click', 'to_settle_accounts_btn')
            else:
                self.driver.switch_to.window(self.driver.window_handles[0])
                return self.po.operation_element('assert', 'to_settle_accounts_btn_assert')
        except IOError:
            print('Error：操作失败')

    # 商品详情，点击“加入购物车”-“去结算”-“去结算”-“提交订单”
    def place_order(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_to_view_price')
                self.po.operation_element('input', 'username', '12300087024')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'add_to_cart')
                self.po.operation_element('click', 'to_settle_accounts')
                self.driver.switch_to.window(self.driver.window_handles[0])
                self.po.operation_element('click', 'to_settle_accounts_btn')
                self.driver.switch_to.window(self.driver.window_handles[0])
                self.driver.execute_script('window.scrollBy(0, 800)')
                sleep(1)
                self.po.operation_element('click', 'place_order')
            else:
                self.driver.switch_to.window(self.driver.window_handles[0])
                return self.po.operation_element('assert', 'place_order_assert')
        except IOError:
            print('Error：操作失败')

    # 商品详情，点击“立即购买”按钮
    def buy_now(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_to_view_price')
                self.po.operation_element('input', 'username', '12300087024')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'buy_now')
            else:
                return self.po.operation_element('assert', 'buy_now_assert')
        except IOError:
            print('Error：操作失败')

    # 商品详情，点击“立即购买”按钮-“提交订单”
    def place_orders(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'login_to_view_price')
                self.po.operation_element('input', 'username', '12300087024')
                self.po.operation_element('input', 'password', '1111aaaa')
                self.po.operation_element('click', 'login_btn')
                self.po.operation_element('click', 'buy_now')
                self.driver.switch_to.window(self.driver.window_handles[0])
                self.driver.execute_script('window.scrollBy(0, 800)')
                sleep(1)
                self.po.operation_element('click', 'place_order')
            else:
                self.driver.switch_to.window(self.driver.window_handles[0])
                return self.po.operation_element('assert', 'place_orders_assert')
        except IOError:
            print('Error：操作失败')
