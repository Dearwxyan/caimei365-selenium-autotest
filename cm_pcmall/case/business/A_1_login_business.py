# coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness


class LoginBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('login', 'url')
        self.config_name = self.oj.get_key_value('login', 'config_name')
        self.node_name = self.oj.get_key_value('login', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.club_username = self.oj.get_key_value('login', 'club_username')
        self.supplier_username = self.oj.get_key_value('login', 'supplier_username')
        self.club_mail = self.oj.get_key_value('login', 'club_mail')
        self.supplier_mail = self.oj.get_key_value('login', 'supplier_mail')
        self.password = self.oj.get_key_value('login', 'password')
        self.username_errors = self.oj.get_key_value('login', 'username_error')
        self.password_errors = self.oj.get_key_value('login', 'password_error')

    # 登录页点击“logo”
    def click_logo(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'logo')
            else:
                return self.po.operation_element('assert', 'click_logo_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录页点击“code”
    def click_code(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'code')
            else:
                return self.po.operation_element('assert', 'click_code_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》手机号为空、密码输入，点击【登录】是否弹出提示
    def username_null(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(password=self.password)
            else:
                return self.po.operation_element('assert', 'username_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》手机号输入，密码为空，点击【登录】是否弹出提示
    def password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(username=self.club_username)
            else:
                return self.po.operation_element('assert', 'password_null_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》手机号输入错误、密码正确，点击【登录】是否弹出提示
    def username_error(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(username=self.username_errors, password=self.password)
            else:
                return self.po.operation_element('assert', 'username_error_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》手机号输入正确、密码错误，点击【登录】是否弹出提示
    def password_error(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(username=self.club_username, password=self.password_errors)
            else:
                return self.po.operation_element('assert', 'password_error_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》’机构‘正确手机号、密码登录，登录成功
    def login_success(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(username=self.club_username, password=self.password)
            elif target == 'assert':
                return self.po.operation_element('assert', 'login_success_assert')
            else:
                return self.po.operation_element('assert', 'login_success_assert1')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》’机构‘正确邮箱、密码登录，登录成功
    def mail_login_success(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(username=self.club_mail, password=self.password)
            elif target == 'assert':
                return self.po.operation_element('assert', 'login_success_assert')
            else:
                return self.po.operation_element('assert', 'login_success_assert1')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录-》’供应商‘正确手机号、密码登录，登录成功
    def supplier_login_success(self, target=None):
        try:
            if target == 'action':
                self.clg.common_login(username=self.supplier_username, password=self.password)
            elif target == 'assert':
                return self.po.operation_element('assert', 'login_success_assert')
            else:
                return self.po.operation_element('assert', 'login_success_assert1')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录页点击“忘记密码”
    def click_forget_password(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'forget_password')
            else:
                return self.po.operation_element('assert', 'click_forget_password_assert')
        except IOError:
            print('Error:抱歉！操作失败')

    # 登录页点击“免费注册”
    def click_free_register(self, target=None):
        try:
            if target == 'action':
                self.po.pe.open()
                self.po.operation_element('click', 'free_register')
            else:
                return self.po.operation_element('assert', 'click_free_register_assert')
        except IOError:
            print('Error:抱歉！操作失败')
