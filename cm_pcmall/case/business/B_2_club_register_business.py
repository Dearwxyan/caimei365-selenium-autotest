# coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
from time import sleep
import random


class ClubRegisterBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.num = str(random.randint(10000, 99999))
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('club_register', 'url')
        self.config_name = self.oj.get_key_value('club_register', 'config_name')
        self.node_name = self.oj.get_key_value('club_register', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.contacts = self.oj.get_key_value('club_register', 'contacts')
        self.cell_phone_number = self.oj.get_key_value('club_register', 'cell_phone_number')
        self.sms_verification_code = self.oj.get_key_value('club_register', 'sms_verification_code')
        self.login_password = self.oj.get_key_value('club_register', 'login_password')
        self.confirm_password = self.oj.get_key_value('club_register', 'confirm_password')
        self.mailbox = self.oj.get_key_value('club_register', 'mailbox')
        self.club_name = self.oj.get_key_value('club_register', 'club_name')
        self.club_abbreviation = self.oj.get_key_value('club_register', 'club_abbreviation')
        self.detailed_address = self.oj.get_key_value('club_register', 'detailed_address')
        self.business_license_no = self.oj.get_key_value('club_register', 'business_license_no')
        self.business_license_picture = self.oj.get_key_value('club_register','business_license_picture')
        self.door_photo = self.oj.get_key_value('club_register', 'door_photo')
        self.qualifications = self.oj.get_key_value('club_register', 'qualifications')
        self.cell_phone_number_errors = self.oj.get_key_value('club_register', 'cell_phone_number_error')
        self.sms_verification_code_errors = self.oj.get_key_value('club_register','sms_verification_code_error')
        self.password_is_inconsistents = self.oj.get_key_value('club_register','password_is_inconsistent')

    # '联系人'为空，提示信息
    def contacts_null(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts='',
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'contacts_null_assert')
        except IOError:
            print('Error:操作失败')

    # '手机号'为空，提示信息
    def cell_phone_number_null(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number='',
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'cell_phone_number_null_assert')
        except IOError:
            print('Error:操作失败')

    # '短信验证码'为空，提示信息
    def sms_verification_code_null(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code='',
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'sms_verification_code_null_assert')
        except IOError:
            print('Error:操作失败')

    # '登录密码'为空，提示信息
    def login_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password='',
                                       confirm_password=self.confirm_password)
            else:
                return self.po.operation_element('assert', 'login_password_null_assert')
        except IOError:
            print('Error:操作失败')

    # '确认密码'为空，提示信息
    def confirm_password_null(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password='')
            else:
                return self.po.operation_element('assert', 'confirm_password_null_assert')
        except IOError:
            print('Error:操作失败')

    # 没有选择协议，提示信息
    def not_click_consent_permission(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password,
                                       permission='no')
            else:
                return self.po.operation_element('assert', 'not_click_consent_permission_assert')
        except IOError:
            print('Error:操作失败')

    # 手机号格式不正确，弹出提示信息
    def cell_phone_number_error(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number_errors) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password,
                                       )
            else:
                return self.po.operation_element('assert', 'cell_phone_number_error_assert')
        except IOError:
            print('Error:操作失败')

    # 短信验证码不正确，弹出提示信息
    def sms_verification_code_error(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code_errors,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password,
                                       )
            else:
                return self.po.operation_element('assert', 'sms_verification_code_error_assert')
        except IOError:
            print('Error:操作失败')

    # 密码不一致，弹出提示信息
    def password_is_inconsistent(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.password_is_inconsistents,
                                       )
            else:
                return self.po.operation_element('assert', 'password_is_inconsistent_assert')
        except IOError:
            print('Error:操作失败')

    # 成功注册（普通）机构，弹出提示信息
    def register_success(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password,
                                       )
            else:
                return self.po.operation_element('assert', 'register_success_assert')
        except IOError:
            print('Error:操作失败')

    # 操作’先跳过，以后再升级‘
    def upgrade_later_button(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password,
                                       type='upgrade_latter'
                                       )
            else:
                return self.po.operation_element('assert', 'upgrade_later_button_assert')
        except IOError:
            print('Error:操作失败')

    # 操作’确认升级并提交审核‘
    def confirm_upgrade_button(self, target=None):
        try:
            if target == 'action':
                self.clg.register_club(contacts=self.contacts,
                                       cell_phone_number=''.join(self.cell_phone_number) + self.num,
                                       sms_verification_code=self.sms_verification_code,
                                       login_password=self.login_password,
                                       confirm_password=self.confirm_password,
                                       mailbox=self.num + ''.join(self.mailbox),
                                       club_name=self.club_name,
                                       club_abbreviation=self.club_abbreviation,
                                       detailed_address=''.join(self.detailed_address) + self.num,
                                       business_license_no=''.join(self.business_license_no) + self.num,
                                       business_license_picture=''.join(self.business_license_picture),
                                       door_photo=''.join(self.door_photo),
                                       qualifications=''.join(self.qualifications),
                                       type='upgrade'
                                       )
            else:
                return self.po.operation_element('assert', 'confirm_upgrade_button_assert')
        except IOError:
            print('Error:操作失败')
