# coding = utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from cm_pcmall.case.business.common.util.operation_json import OperationJson
from cm_pcmall.case.business.A_0_common_fun import CommonLoginBusiness
from time import sleep
import random


class NotLoginIndexBusiness(object):
    def __init__(self, driver):
        self.driver = driver
        self.num = str(random.randint(10000, 99999))
        self.data_name = 'pcmalldata.json'
        self.oj = OperationJson(self.data_name)
        self.url = self.oj.get_key_value('not_login_index', 'url')
        self.config_name = self.oj.get_key_value('not_login_index', 'config_name')
        self.node_name = self.oj.get_key_value('not_login_index', 'node_name')
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)
        self.clg = CommonLoginBusiness(self.driver, self.url, self.config_name, self.node_name)
        self.search_product = self.oj.get_key_value('not_login_index', 'search_product')
        self.search_supplier = self.oj.get_key_value("not_login_index", "search_supplier")
        self.search_project_instrument = self.oj.get_key_value("not_login_index", "project_instrument")

    # 头部-》点击“登录”链接
    def login_link(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='login_link')
            else:
                return self.po.operation_element('assert', 'login_link_assert')
        except IOError:
            print('Error：操作失败')

    # 头部-》点击“注册”链接
    def register_link(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='register_link')
            else:
                return self.po.operation_element('assert', 'register_link_assert')
        except IOError:
            print('Error：操作失败')

    # 头部-》点击“购物车”链接
    def shopping_cart(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='shopping_cart')
            else:
                return self.po.operation_element('assert', 'shopping_cart_assert')
        except IOError:
            print('Error：操作失败')

    # 头部-》点击“logo”
    def click_logo(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='click_logo')
            else:
                return self.po.operation_element('assert', 'click_logo_assert')
        except IOError:
            print('Error：操作失败')

    # 搜索功能-》搜索“产品”
    def choose_products_search(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='search',
                                         choose='products',
                                         search_content=self.search_product)
            elif target == 'assert':
                return self.po.operation_element('assert', 'choose_products_search_assert')
            else:
                return self.po.operation_element('assert', 'choose_products_search_assert2')
        except IOError:
            print('Error：操作失败')

    # 搜索功能-》搜索“供应商”
    def choose_supplier_search(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='search',
                                         choose='supplier',
                                         search_content=self.search_supplier)
            elif target == 'assert':
                return self.po.operation_element('assert', 'choose_supplier_search_assert')
            else:
                return self.po.operation_element('assert', 'choose_supplier_search_assert2')
        except IOError:
            print('Error：操作失败')

    # 搜索功能-》搜索“项目仪器”
    def choose_project_instrument_search(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='search',
                                         choose='project_instrument',
                                         search_content=self.search_project_instrument)
            else:
                return self.po.operation_element('assert', 'choose_project_instrument_search_assert')
        except IOError:
            print('Error：操作失败')

    # 搜索功能-》搜索“热搜词”
    def hot_search_words(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='hot_search_words')
            else:
                return self.po.operation_element('assert', 'hot_search_words_assert')
        except IOError:
            print('Error：操作失败')

    # 点击“采美公众号”
    def official_account(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='official_account')
            else:
                return self.po.operation_element('assert', 'official_account_assert')
        except IOError:
            print('Error：操作失败')

    # 点击“采美小程序”
    def caimei_small_program(self, target=None):
        try:
            if target == 'action':
                self.clg.not_login_index(type='caimei_small_program')
            else:
                return self.po.operation_element('assert', 'caimei_small_program_assert')
        except IOError:
            print('Error：操作失败')
