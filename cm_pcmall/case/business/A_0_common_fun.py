# coding=utf-8
from cm_pcmall.case.business.common.page_operation import PageOperation
from selenium.webdriver.common.action_chains import ActionChains
import random
from time import sleep


class CommonLoginBusiness(object):
    def __init__(self, driver, url, config_name, node_name):
        self.driver = driver
        self.url = url
        self.config_name = config_name
        self.node_name = node_name
        self.num = str(random.randint(1000, 9999))
        self.po = PageOperation(self.driver, self.url, self.config_name, self.node_name)

    # 全局登录功能
    def common_login(self, username=None, password=None, account_type=None, user_type=None):
        self.po.pe.open()
        if username is None and password is None:
            if user_type == 'club' and account_type == 'cell_phone':
                self.po.operation_element('input', 'username', '15917362709')
                self.po.operation_element('input', 'password', '1111aaaa')
            else:
                self.po.operation_element('input', 'username', '1300162521@qq.com')
                self.po.operation_element('input', 'password', '1111aaaa')
            if user_type == 'supplier' and account_type == 'cell_phone':
                self.po.operation_element('input', 'username', '18476937515')
                self.po.operation_element('input', 'password', '1111aaaa')
            else:
                self.po.operation_element('input', 'username', '3294428324@qq.com')
                self.po.operation_element('input', 'password', '1111aaaa')
        else:
            if username is None:
                username = ''
                self.po.operation_element('input', 'username', username)
                self.po.operation_element('input', 'password', password)
            elif password is None:
                password = ''
                self.po.operation_element('input', 'username', username)
                self.po.operation_element('input', 'password', password)
            else:
                self.po.operation_element('input', 'username', username)
                self.po.operation_element('input', 'password', password)
        self.operation_slider()
        self.po.operation_element('click', 'login_btn')

    # 操作滑块验证
    def operation_slider(self):
        dragger = self.po.pe.get_element('value', 'slider_scroll')
        action = ActionChains(self.driver)  # 通过click_and_hold()方法对滑块按下鼠标左键
        action.click_and_hold(dragger).perform()  # 鼠标左键按下不放
        action.move_by_offset(348, 0).perform()  # 平行移动鼠标
        return '操作成功'

    # 优惠券弹窗
    def operation_coupon(self):
        self.po.operation_element('click', 'coupon_pop_up')

    # 修改密码
    def change_password(self, type=None, cell_phone_number=None, sms_verification_code=None, new_password=None,
                        confirm_password=None):
        self.po.pe.open()
        if type == 'mail':
            self.po.operation_element('click', 'modify_with_mailbox')
        self.po.operation_element('input', 'cell_phone_number', cell_phone_number)
        self.po.operation_element('input', 'sms_verification_code', sms_verification_code)
        self.po.operation_element('input', 'new_password', new_password)
        self.po.operation_element('input', 'confirm_password', confirm_password)
        self.po.operation_element('click', 'submit_button')

    # 绑定运营人员
    def bind_operators(self, account_number=None, login_password=None, full_name=None, cell_phone_number=None,
                       sms_verification_code=None):
        self.po.pe.open()
        self.po.operation_element('input', 'account_number', account_number)
        self.po.operation_element('input', 'login_password', login_password)
        self.po.operation_element('input', 'full_name', full_name)
        self.po.operation_element('input', 'cell_phone_number', cell_phone_number)
        self.po.operation_element('input', 'sms_verification_code', sms_verification_code)
        self.po.operation_element('click', 'submit_button')

    # 注册机构
    def register_club(self, type=None, permission=None, contacts=None, cell_phone_number=None, sms_verification_code=None,
                      login_password=None, confirm_password=None, mailbox=None, club_name=None, club_abbreviation=None,
                      detailed_address=None, business_license_no=None, business_license_picture=None, door_photo=None,
                      qualifications=None):
        self.po.pe.open()
        self.po.operation_element('input', 'contacts', contacts)
        self.po.operation_element('input', 'cell_phone_number', cell_phone_number)
        self.po.operation_element('input', 'sms_verification_code', sms_verification_code)
        self.po.operation_element('input', 'login_password', login_password)
        self.po.operation_element('input', 'confirm_password', confirm_password)
        self.po.operation_element('click', 'click_consent_permission')
        if permission == 'no':
            self.po.operation_element('click', 'click_consent_permission')
        self.po.operation_element('click', 'click_register_button')
        if type == 'upgrade_latter':
            sleep(3)
            self.po.operation_element('click', 'upgrade_later_button')
        if type == 'upgrade':
            sleep(3)
            self.po.operation_element('input', 'mailbox', mailbox)
            self.po.operation_element('input', 'club_name', club_name)
            self.po.operation_element('input', 'club_abbreviation', club_abbreviation)
            self.po.operation_element('click', 'address_provinces')
            self.po.operation_element('click', 'address_province_value')
            self.po.operation_element('click', 'address_city')
            self.po.operation_element('click', 'address_city_value')
            self.po.operation_element('click', 'address_area')
            self.po.operation_element('click', 'address_area_value')
            self.po.operation_element('input', 'detailed_address', detailed_address)
            self.po.operation_element('input', 'business_license_no', business_license_no)
            self.po.operation_element('input', 'business_license_picture', business_license_picture)
            self.po.operation_element('input', 'door_photo', door_photo)
            self.po.operation_element('click', 'medical_beauty')
            self.po.operation_element('click', 'clinic')
            self.po.operation_element('input', 'qualifications', qualifications)
            self.po.operation_element('click', 'main_contents')
            self.po.operation_element('click', 'confirm_upgrade_button')

    # 注册供应商
    def register_supplier(self, cell_phone_number=None, sms_verification_code=None, login_password=None,
                          confirm_password=None, corporate_name=None, contacts=None, mailbox=None,
                          company_abbreviation=None, detailed_address=None, business_license_no=None,
                          business_license_picture=None, official_website_address=None, wechat_official_account=None,
                          wechat_applet=None, product_description=None, company_profile=None):
        self.po.pe.open()
        self.po.operation_element('click', 'agree')
        self.po.operation_element('input', 'cell_phone_number', cell_phone_number)
        self.po.operation_element('input', 'sms_verification_code', sms_verification_code)
        self.po.operation_element('input', 'login_password', login_password)
        self.po.operation_element('input', 'confirm_password', confirm_password)
        self.po.operation_element('input', 'corporate_name', corporate_name)
        self.po.operation_element('input', 'contacts', contacts)
        self.po.operation_element('input', 'mailbox', mailbox)
        self.po.operation_element('input', 'company_abbreviation', company_abbreviation)
        self.po.operation_element('click', 'address_province')
        self.po.operation_element('click', 'address_province_value')
        self.po.operation_element('click', 'address_city')
        self.po.operation_element('click', 'address_city_value')
        self.po.operation_element('click', 'address_area')
        self.po.operation_element('click', 'address_area_value')
        self.po.operation_element('input', 'detailed_address', detailed_address)
        self.po.operation_element('input', 'business_license_no', business_license_no)
        self.po.operation_element('input', 'business_license_picture', business_license_picture)
        self.po.operation_element('click', 'medical_care')
        self.po.operation_element('click', 'medical_type_one')
        self.po.operation_element('click', 'product')
        self.po.operation_element('input', 'official_website_address', official_website_address)
        self.po.operation_element('input', 'wechat_official_account', wechat_official_account)
        self.po.operation_element('input', 'wechat_applet', wechat_applet)
        self.po.operation_element('input', 'product_description', product_description)
        self.po.operation_element('input', 'company_profile', company_profile)
        self.po.operation_element('click', 'check_consent_agreement')
        self.po.operation_element('click', 'agree_submit_review')

    # 用户没有登录首页
    def not_login_index(self, type=None, choose=None, search_content=None):
        self.po.pe.open()
        self.operation_coupon()
        if type == "login_link":
            self.po.operation_element('click', 'login_link')
        if type == "register_link":
            self.po.operation_element('click', 'register_link')
        if type == "shopping_cart":
            self.po.operation_element('click', 'shopping_cart')
        if type == "click_logo":
            self.po.operation_element('click', 'click_logo')
        if type == 'search':
            self.po.operation_element('click', 'click_search_options')
            if choose == 'products':
                self.po.operation_element('click', 'choose_products')
            if choose == 'supplier':
                self.po.operation_element('click', 'choose_supplier')
            if choose == 'project_instrument':
                self.po.operation_element('click', 'choose_project_instrument')
            self.po.operation_element('input', 'search_box', search_content)
            self.po.operation_element('click', 'search_button')
        if type == 'hot_search_words':
            self.po.operation_element('click', 'hot_search_words')
        if type == 'official_account':
            self.po.operation_element('click', 'official_account')
        if type == 'caimei_small_program':
            self.po.operation_element('click', 'caimei_small_program')

    # 机构登录系统
    def club_signed(self, type=None, order_type=None, upload_avatar=None, search_type=None,
                    order_number_search=None, order_start_time=None, order_end_time=None,
                    operate=None):
        if type == "select_my_beauty":
            self.po.operation_element('click', 'nickname_options')
            self.po.operation_element('click', 'select_my_beauty')
        if type == "club_information":
            self.po.operation_element('click', 'nickname_options')
            self.po.operation_element('click', 'select_club_information')
        if type == "log_out":
            self.po.operation_element('click', 'nickname_options')
            self.po.operation_element('click', 'select_log_out')
        if type == "shopping_cart":
            self.po.operation_element('click', 'shopping_cart')
        if type == "menu_my_beauty":
            self.po.operation_element('click', 'menu_my_beauty')
        if type == "upload_avatar":
            self.po.operation_element('input', 'upload_avatar', upload_avatar)
        if type == "improve_immediately":
            self.po.operation_element('click', 'improve_immediately')
        if type == "my_order":
            self.po.operation_element('click', 'menu_my_deal')
            self.po.operation_element('click', 'my_order')
            if order_type== "to_be_confirmed":
                self.po.operation_element('click', 'click_to_be_confirmed')
            if order_type == "to_be_paid":
                self.po.operation_element('click', 'click_to_be_paid')
            if order_type == "to_be_delivered":
                self.po.operation_element('click', 'click_to_be_delivered')
            if order_type == "delivered":
                self.po.operation_element('click', 'click_delivered')
            if order_type == "return_goods":
                self.po.operation_element('click', 'click_return_goods')
            if order_type == "contact_customer_service":
                self.po.operation_element('click', 'click_contact_customer_service')
            if search_type == "order_number_search":
                self.po.operation_element('input', 'order_number_search', order_number_search)
                self.po.operation_element('click', 'search_button')
            if search_type == "order_time":
                self.po.operation_element('input', 'order_start_time', order_start_time)
                self.po.operation_element('input', 'order_end_time', order_end_time)
                self.po.operation_element('click', 'search_button')
            if operate == "view_Logistics":
                self.po.operation_element('click', 'view_Logistics')
            if operate == "order_details":
                self.po.operation_element('click', 'order_details')
            if operate== "payment_order":
                self.po.operation_element('click', 'payment_order')



