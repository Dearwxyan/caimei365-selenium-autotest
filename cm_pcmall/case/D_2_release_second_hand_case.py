# coding = utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.D_2_release_second_hand_business import ReleaseSecondHandBusiness
from time import sleep


class ReleaseSecondHandCase(StartEnd):
    def test_D01_secondhand_market_introduce(self):
        '''
        检查点击‘二手市场介绍’是否跳转‘二手市场介绍’页面
        :return:
        '''
        rhb = ReleaseSecondHandBusiness(self.driver)
        rhb.secondhand_market_introduce('action')
        om().screen_img(self.driver, 'D_2_01_secondhand_market_introduce.png')
        self.assertEqual(rhb.secondhand_market_introduce('assert'), '二手市场')

    def test_D02_release_second_hand(self):
        '''
        检查‘发布二手’功能是否正常
        :return:
        '''
        rhb = ReleaseSecondHandBusiness(self.driver)
        rhb.release_second_hand('action')
        om().screen_img(self.driver, 'D_2_02_release_second_hand.png')
        self.assertEqual(rhb.release_second_hand('assert'), '发布二手商品，采美需要收取您每个商品100元的展示费，展示期为3个月支付完成后，商品会在1-2个工作日内进行审核，审核通过后，商品会立即上线')

    def test_D03_release_second_hands(self):
        '''
        检查‘发布二手’功能是否正常
        :return:
        '''
        rhb = ReleaseSecondHandBusiness(self.driver)
        rhb.release_second_hands('action')
        om().screen_img(self.driver, 'D_2_03_release_second_hands.png')
        self.assertEqual(rhb.release_second_hands('assert'), '发布二手商品，采美需要收取您每个商品100元的展示费，展示期为3个月支付完成后，商品会在1-2个工作日内进行审核，审核通过后，商品会立即上线')

