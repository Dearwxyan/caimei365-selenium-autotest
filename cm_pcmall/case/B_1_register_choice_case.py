# coding=utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.B_1_register_choice_business import RegisterChoiceBusiness
from time import sleep


class RegisterChoiceCase(StartEnd):
    def test_B00_click_logo(self):
        '''
        检查点击登录页”logo“是否跳转【首页】
        :return:
        '''
        rcb = RegisterChoiceBusiness(self.driver)
        rcb.click_logo('action')
        sleep(2)
        om().screen_img(self.driver, 'B_1_00_login_click_logo.png')
        self.assertEqual(rcb.click_logo(), '欢迎来到采美采购服务平台')

    def test_B01_click_login_btn(self):
        '''
        检查点击”登录“链接是否跳转登录页面
        :return:
        '''
        rcb = RegisterChoiceBusiness(self.driver)
        rcb.click_login('action')
        sleep(2)
        om().screen_img(self.driver, 'B_1_01_click_login_btn.png')
        self.assertEqual(rcb.click_login(), '欢迎登录\n生美 / 医美采购服务平台')

    def test_B02_click_register_club(self):
        '''
        检查选择点击注册‘机构’是否跳转注册‘机构’页面
        :return:
        '''
        rcb = RegisterChoiceBusiness(self.driver)
        rcb.click_register_club('action')
        sleep(2)
        om().screen_img(self.driver, 'B_1_02_click_register_club.png')
        self.assertEqual(rcb.click_register_club(), '欢迎机构入驻')

    def test_B03_click_register_supplier(self):
        '''
        检查选择点击注册‘供应商’是否跳转注册‘供应商’页面
        :return:
        '''
        rcb = RegisterChoiceBusiness(self.driver)
        rcb.click_register_supplier('action')
        sleep(2)
        om().screen_img(self.driver, 'B_1_03_click_register_supplier.png')
        self.assertEqual(rcb.click_register_supplier(), '供应商注册需知')


