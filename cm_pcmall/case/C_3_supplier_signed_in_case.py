# coding = utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.C_3_supplier_signed_in_business import SupplierSignedInBusiness
from time import sleep


class ClubSignedInCase(StartEnd):
    def test_C01_select_my_beauty(self):
        '''
        检查'供应商'登录，点击’我的采美‘是否跳转个人中心页面
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.select_my_beauty('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_01_select_my_beauty.png')
        self.assertEqual(ssb.select_my_beauty('assert1'), '我的交易')
        self.assertEqual(ssb.select_my_beauty('assert2'), '我的店铺')
        self.assertEqual(ssb.select_my_beauty('assert3'), '管理中心')
        self.assertEqual(ssb.select_my_beauty(), '账户设置')

    def test_C02_select_store_management(self):
        '''
        检查'供应商'登录，点击‘店铺管理’是否跳转‘店铺管理’页面
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.select_store_management('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_02_select_store_management.png')
        self.assertEqual(ssb.select_store_management('assert1'), '深圳市肤白貌美服务有限公司')
        self.assertEqual(ssb.select_store_management(), '产品展示 公司介绍')

    def test_C03_select_log_out(self):
        '''
        检查'供应商'登录，点击‘退出登录’是否正常退出“供应商”登录
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.select_log_out('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_03_select_log_out.png')
        self.assertEqual(ssb.select_log_out(), '登录')

    def test_C04_upload_avatar(self):
        '''
        检查'供应商'登录，操作‘上传头像’功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.upload_avatar('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_04_upload_avatar.png')

    def test_C05_improve_immediately(self):
        '''
        检查'供应商'登录，操作‘完善资料’功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.improve_immediately('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_05_improve_immediately.png')
        self.assertEqual(ssb.improve_immediately(), '修改成功')

    def test_C06_order_number_query(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘订单编号’查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.order_number_query('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_06_order_number_query.png')
        self.assertEqual(ssb.order_number_query(), '订单编号(标识)：H16209773262340301')

    def test_C07_buyer_name_query(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘买家名称’查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.buyer_name_query('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_07_buyer_name_query.png')
        # self.assertEqual(ssb.buyer_name_query(), '小傻瓜')

    def test_C08_delivery_status_one(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘结算状态’-“待结算”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.delivery_status_one('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_08_delivery_status_one.png')
        self.assertEqual(ssb.delivery_status_one(), '结算状态：  待结算')

    def test_C09_delivery_status_two(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘结算状态’-“部分结算”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.delivery_status_two('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_09_delivery_status_two.png')
        self.assertEqual(ssb.delivery_status_two(), '结算状态：  部分结算')

    def test_C10_delivery_status_three(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘结算状态’-“已结算”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.delivery_status_three('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_10_delivery_status_three.png')
        self.assertEqual(ssb.delivery_status_three(), '结算状态：  已结算')

    def test_C11_settlement_status_one(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘发货状态’-“待发货”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_status_one('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_11_settlement_status_one.png')
        self.assertEqual(ssb.settlement_status_one(), '发货状态：  待发货')

    def test_C12_settlement_status_two(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘发货状态’-“部分发货”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_status_two('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_12_settlement_status_two.png')
        self.assertEqual(ssb.settlement_status_two(), '发货状态：  部分发货')

    def test_C13_settlement_status_three(self):
        '''
        检查'供应商'登录，操作‘我的交易-》我的订单’，‘发货状态’-“部分发货”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_status_three('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_13_settlement_status_three.png')
        self.assertEqual(ssb.settlement_status_three(), '发货状态：  已发货')

    def test_C14_settlement_order_number(self):
        '''
        检查'供应商'登录，操作‘我的交易-》结算管理’，‘订单编号’查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_order_number('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_14_settlement_order_number.png')
        self.assertEqual(ssb.settlement_order_number(), 'X16221881460891701')

    def test_C15_settlement_buyer_name(self):
        '''
        检查'供应商'登录，操作‘我的交易-》结算管理’，‘买家名称’查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_buyer_name('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_15_settlement_buyer_name.png')
        self.assertEqual(ssb.settlement_buyer_name(), 'OMG')

    def test_C16_settlement_delivery_status_one(self):
        '''
        检查'供应商'登录，操作‘我的交易-》结算管理’，‘结算状态’-“待结算”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_delivery_status_one('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_16_settlement_delivery_status_one.png')
        self.assertEqual(ssb.settlement_delivery_status_one(), '待结算')

    def test_C17_settlement_delivery_status_two(self):
        '''
        检查'供应商'登录，操作‘我的交易-》结算管理’，‘结算状态’-“部分结算”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_delivery_status_two('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_17_settlement_delivery_status_two.png')
        self.assertEqual(ssb.settlement_delivery_status_two(), '部分结算')

    def test_C18_settlement_delivery_status_three(self):
        '''
        检查'供应商'登录，操作‘我的交易-》结算管理’，‘结算状态’-“已结算”查询功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.settlement_delivery_status_three('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_18_settlement_delivery_status_three.png')
        self.assertEqual(ssb.settlement_delivery_status_three(), '已结算')

    def test_C19_view_store(self):
        '''
        检查'供应商'登录，操作‘我的店铺-》查看店铺’，‘查看店铺”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.view_store('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_19_view_store.png')
        self.assertEqual(ssb.view_store(), '深圳市肤白貌美服务有限公司')

    def test_C20_dress_up_home_page(self):
        '''
        检查'供应商'登录，操作‘我的店铺-》装扮主页’，‘装扮主页”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.dress_up_home_page('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_20_dress_up_home_page.png')
        self.assertEqual(ssb.dress_up_home_page(), '我的店铺 > 装扮主页')

    def test_C21_release_product(self):
        '''
        检查'供应商'登录，操作‘我的店铺-》发布商品’，‘发布商品”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.release_product('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_21_release_product.png')
        self.assertEqual(ssb.release_product(), '提交成功，等待审核')

    def test_C22_preview_product(self):
        '''
        检查'供应商'登录，操作‘我的店铺-》发布商品’，‘预览商品”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.preview_product('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_22_preview_product.png')
        # self.assertEqual(ssb.preview_product(), '法国CRISTAL绿塑冷冻溶脂32632')

    def test_C23_my_product(self):
        '''
        检查'供应商'登录，操作‘我的店铺-》我的商品’，‘我的商品”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.my_product('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_23_my_product.png')
        self.assertEqual(ssb.my_product(), '我的店铺 > 我的商品')

    def test_C24_submit_brand(self):
        '''
        检查'供应商'登录，操作‘我的店铺-》品牌管理’，‘提交新品牌”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.submit_brand('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_24_submit_brand.png')
        self.assertEqual(ssb.submit_brand(), '提交成功，我们将会在1-2个工作日内审核')

    def test_C25_submit_information(self):
        '''
        检查'供应商'登录，操作‘管理中心-》资料信息’，‘提交新资料信息”功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.submit_information('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_25_submit_information.png')
        self.assertEqual(ssb.submit_information(), '修改成功')

    def test_C26_staff_management(self):
        '''
        检查'供应商'登录，操作‘管理中心-》员工管理’，‘添加运营人员’功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.staff_management('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_26_staff_management.png')
        self.assertEqual(ssb.staff_management(), '添加成功')

    def test_C27_reset_password(self):
        '''
        检查'供应商'登录，操作‘账户设置-》重置密码’，‘手机号修改密码’功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.reset_password('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_27_reset_password.png')
        self.assertEqual(ssb.reset_password(), '密码修改成功~')

    def test_C28_switch_mailbox(self):
        '''
        检查'供应商'登录，操作‘账户设置-》重置密码’，‘邮箱修改密码’功能是否正常
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.switch_mailbox('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_28_switch_mailbox.png')
        self.assertEqual(ssb.switch_mailbox(), '密码修改成功~')

    def test_C29_change_mobile_phone_number(self):
        '''
        检查'供应商'登录，操作‘账户设置-》更换手机’，‘更换手机号’功能是否正常（更换手机号为：18777777776）
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.change_mobile_phone_number('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_29_change_mobile_phone_number.png')
        self.assertEqual(ssb.change_mobile_phone_number(), '修改成功~')

    def test_C30_change_mobile_phone_numbers(self):
        '''
        检查'供应商'登录，操作‘账户设置-》更换手机’，‘更换手机号’功能是否正常（更换手机号为：18666666666）
        :return:
        '''
        ssb = SupplierSignedInBusiness(self.driver)
        ssb.change_mobile_phone_numbers('action')
        sleep(2)
        om().screen_img(self.driver, 'C_3_30_change_mobile_phone_numbers.png')
        self.assertEqual(ssb.change_mobile_phone_numbers(), '修改成功~')