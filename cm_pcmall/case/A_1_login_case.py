# coding=utf-8
from time import sleep
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.A_1_login_business import LoginBusiness


class LoginCase(StartEnd):
    def test_A01_click_logo(self):
        '''
        检查点击登录页”logo“是否跳转【首页】
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.click_logo('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_01_login_click_logo.png')
        self.assertEqual(lb.click_logo(), '欢迎来到采美采购服务平台')

    def test_A02_click_code(self):
        '''
        检查点击登录页”code“是否切换为【微信二维码登录】
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.click_code('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_02_login_click_code.png')
        self.assertEqual(lb.click_code(), '账号登录')

    def test_A03_username_null(self):
        '''
        检查用户名为空、密码输入，点击【登录】是否弹出”请提供正确的邮箱或手机号码“
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.username_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_03_login_username_null.png')
        self.assertEqual(lb.username_null(), '邮箱或手机号不能为空')

    def test_A04_password_null(self):
        '''
        检查输入账号，密码为空，点击【登录】是否弹出”请提供正确的邮箱或手机号码“
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_04_login_password_null.png')
        self.assertEqual(lb.password_null(), '密码不能为空')

    def test_A05_username_error(self):
        '''
        检查用户名错误、密码正确，点击【登录】是否弹出”输入的密码和账户名不匹配“
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.username_error('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_05_login_username_error.png')
        self.assertEqual(lb.username_error(), '输入的密码和账户名不匹配')

    def test_A06_password_error(self):
        '''
        检查用户名正确、密码错误，点击【登录】是否弹出”输入的密码和账户名不匹配“
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.password_error('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_06_login_password_error.png')
        self.assertEqual(lb.password_error(), '输入的密码和账户名不匹配')

    def test_A07_login_success(self):
        '''
        检查’机构‘用户名、密码正确，点击【登录】是否正常登录进入个人中心
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.login_success('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_07_login_success.png')
        self.assertEqual(lb.login_success('assert'), '是否可见')
        self.assertEqual(lb.login_success(), '我的采美')

    def test__A08_mail_login_success(self):
        '''
        检查’机构‘邮箱、密码正确，点击【登录】是否正常登录进入个人中心
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.mail_login_success('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_08_mail_login_success.png')
        self.assertEqual(lb.mail_login_success('assert'), '是否可见')
        self.assertEqual(lb.mail_login_success(), '我的采美')

    def test_A09_supplier_login_success(self):
        '''
        检查’机构‘邮箱、密码正确，点击【登录】是否正常登录进入个人中心
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.supplier_login_success('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_09_supplier_login_success.png')
        self.assertEqual(lb.supplier_login_success('assert'), '深圳市美轮美奂服务有限公司')
        self.assertEqual(lb.supplier_login_success(), '我的采美')

    def test_A10_click_forget_password(self):
        '''
        检查点击“忘记密码？”是否跳转【密码修改】页面
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.click_forget_password('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_10_click_forget_password.png')
        self.assertEqual(lb.click_forget_password(), '使用手机修改')

    def test_A11_click_free_register(self):
        '''
        检查点击“免费注册”是否跳转【选择注册账号类型】页面
        :return:
        '''
        lb = LoginBusiness(self.driver)
        lb.click_free_register('action')
        sleep(2)
        om().screen_img(self.driver, 'A_1_11_click_free_register.png')
        self.assertEqual(lb.click_free_register(), '请选择注册的账号类型')
