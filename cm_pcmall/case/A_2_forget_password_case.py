# coding=utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.A_2_forget_password_business import ForgetPasswordBusiness
from time import sleep


class ForgetPasswordCase(StartEnd):
    def test_A00_click_logo(self):
        '''
        检查点击登录页”logo“是否跳转【首页】
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.click_logo('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_00_login_click_logo.png')
        self.assertEqual(fpc.click_logo(), '欢迎来到采美采购服务平台')

    def test_A01_click_login_btn(self):
        '''
        检查‘修改密码’页面，点击“登录”是否跳转登录页面
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.click_login_btn('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_01_click_login_btn.png')
        self.assertEqual(fpc.click_login_btn(), '欢迎登录\n生'
                                                '美 / 医美采购服务平台')

    def test_A02_cell_phone_number_null(self):
        '''
        检查‘修改密码’页面，‘手机号’输入框为空，点击“登录”是否弹出提示‘手机号码不能为空’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.cell_phone_number_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_02_cell_phone_number_null.png')
        self.assertEqual(fpc.cell_phone_number_null(), '手机号码不能为空')

    def test_A03_sms_verification_code_null(self):
        '''
        检查‘修改密码’页面，‘短信验证码’输入框为空，点击“登录”是否弹出提示‘短信验证码不能为空’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.sms_verification_code_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_03_sms_verification_code_null.png')
        self.assertEqual(fpc.sms_verification_code_null(), '短信验证码不能为空')

    def test_A04_new_password_null(self):
        '''
        检查‘修改密码’页面，‘新密码’输入框为空，点击“登录”是否弹出提示‘新密码,为8-16位数字和字母组合不能为空’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.new_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_04_new_password_null.png')
        self.assertEqual(fpc.new_password_null(), '新密码,为8-16位数字和字母组合不能为空')

    def test_A05_confirm_password_null(self):
        '''
        检查‘修改密码’页面，‘确认密码’输入框为空，点击“登录”是否弹出提示‘不能为空’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.confirm_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_05_confirm_password_null.png')
        self.assertEqual(fpc.confirm_password_null(), '不能为空')

    def test_A06_cell_phone_number_error(self):
        '''
        检查‘修改密码’页面，手机号不正确，点击“登录”是否弹出提示‘手机号码格式不正确’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.cell_phone_number_error('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_06_cell_phone_number_error.png')
        self.assertEqual(fpc.cell_phone_number_error(), '手机号码格式不正确')

    def test_A07_ms_verification_code_error(self):
        '''
        检查‘修改密码’页面，验证码不正确，点击“登录”是否弹出提示‘验证码错误’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.ms_verification_code_error('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_07_ms_verification_code_error.png')
        self.assertEqual(fpc.ms_verification_code_error(), '验证码错误')

    def test_A08_password_not_conform_rules(self):
        '''
        检查‘修改密码’页面，密码不符合规则，点击“登录”是否弹出提示‘8-16位数字或字母或字符’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.password_not_conform_rules('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_08_password_not_conform_rules.png')
        self.assertEqual(fpc.password_not_conform_rules(), '8-16位数字或字母或字符')

    def test_A09_password_is_inconsistent(self):
        '''
        检查‘修改密码’页面，密码不符合规则，点击“登录”是否弹出提示‘两次密码输入不一致’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.password_is_inconsistent('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_09_password_is_inconsistent.png')
        self.assertEqual(fpc.password_is_inconsistent(), '两次密码输入不一致')

    def test_A10_club_change_password_success(self):
        '''
        检查"机构"是否能正常“修改密码”，成功提示“成功找回密码！”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.club_change_password_success('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_10_club_change_password_success.png')
        self.assertEqual(fpc.club_change_password_success(), '密码修改成功~')

    def test_A11_supplier_change_password_success(self):
        '''
        检查"供应商"是否能正常“修改密码”，成功提示“成功找回密码！”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.supplier_change_password_success('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_11_supplier_change_password_success.png')
        self.assertEqual(fpc.supplier_change_password_success(), '密码修改成功~')

    def test_A12_mailbox_null(self):
        '''
        检查"使用邮箱找回密码"，邮箱输入框为空，是否弹出提示“邮箱不能为空”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.mailbox_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_12_mailbox_null.png')
        self.assertEqual(fpc.mailbox_null(), '邮箱不能为空')

    def test_A13_email_verification_code_null(self):
        '''
        检查"使用邮箱找回密码"，邮箱验证码输入框为空，是否弹出提示“邮箱验证码不能为空”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.email_verification_code_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_13_email_verification_code_null.png')
        self.assertEqual(fpc.email_verification_code_null(), '邮箱验证码不能为空')

    def test_A14_login_password_null(self):
        '''
        检查"使用邮箱找回密码"，登录密码输入框为空，是否弹出提示“新密码,为8-16位数字和字母组合不能为空”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.login_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_14_login_password_null.png')
        self.assertEqual(fpc.login_password_null(), '新密码,为8-16位数字和字母组合不能为空')

    def test_A15_confirms_password_null(self):
        '''
        检查"使用邮箱找回密码"，确认密码输入框为空，是否弹出提示“不能为空”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.confirms_password_null('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_15_confirms_password_null.png')
        self.assertEqual(fpc.confirms_password_null(), '不能为空')

    def test_A16_mail_error(self):
        '''
        检查"使用邮箱找回密码"，邮箱不正确，是否弹出提示“请输入有效的邮箱地址”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.mail_error('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_16_mail_error.png')
        self.assertEqual(fpc.mail_error(), '请输入有效的邮箱地址')

    def test_A17_email_verification_code_error(self):
        '''
        检查"使用邮箱找回密码"，邮箱验证码不正确，是否弹出提示“验证码错误”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.email_verification_code_error('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_17_email_verification_code_error.png')
        self.assertEqual(fpc.email_verification_code_error(), '验证码错误')

    def test_A18_password_not_standard(self):
        '''
        检查"使用邮箱找回密码"，密码不符合规范，是否弹出提示“8-16位数字或字母或字符”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.password_not_standard('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_18_password_not_standard.png')
        self.assertEqual(fpc.password_not_standard(), '8-16位数字或字母或字符')

    def test_A19_passwords_is_inconsistent(self):
        '''
        检查"使用邮箱找回密码"，密码不一致，是否弹出提示“两次密码输入不一致”
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.passwords_is_inconsistent('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_19_passwords_is_inconsistent.png')
        self.assertEqual(fpc.passwords_is_inconsistent(), '两次密码输入不一致')

    def test_A20_club_change_passwords_success(self):
        '''
        检查"使用邮箱找回密码"，[机构]正常修改密码 提示‘成功找回密码！’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.club_change_passwords_success('action')
        sleep(2)
        om().screen_img(self.driver, 'A_2_20_club_change_passwords_success.png')
        self.assertEqual(fpc.club_change_passwords_success(), '密码修改成功~')

    def test_A21_supplier_change_passwords_success(self):
        '''
        检查"使用邮箱找回密码"，[供应商]正常修改密码 提示‘成功找回密码！’
        :return:
        '''
        fpc = ForgetPasswordBusiness(self.driver)
        fpc.supplier_change_passwords_success('action')
        sleep(2)
        om().screen_img('cm_pcmall', self.driver, 'A_2_21_supplier_change_passwords_success.png')
        self.assertEqual(fpc.supplier_change_passwords_success(), '密码修改成功~')
