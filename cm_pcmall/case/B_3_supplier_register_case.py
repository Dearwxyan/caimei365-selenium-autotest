# coding=utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.B_3_supplier_register_business import SupplierRegisterBusiness
from time import sleep


class SupplierRegisterCase(StartEnd):
    def test_B01_contacts_null(self):
        '''
        检查点击‘登录’链接是否跳转登录页面
        :return:
        '''
        srb = SupplierRegisterBusiness(self.driver)
        srb.click_login('action')
        sleep(2)
        om().screen_img(self.driver, 'B_3_01_contacts_null.png')
        self.assertEqual(srb.click_login(), '欢迎登录\n生美 / 医美采购服务平台')

    def test_B02_agree_submit_review(self):
        '''
        检查‘注册’供应商并提交审核信息
        :return:
        '''
        srb = SupplierRegisterBusiness(self.driver)
        srb.agree_submit_review('action')
        sleep(2)
        om().screen_img(self.driver, 'B_30_02_agree_submit_review.png')
        self.assertEqual(srb.agree_submit_review(), '您的资料已提交成功，系统将在1-2个工作日内进行审核~')