# coding = utf-8
from cm_pcmall.case.business.common.myunit import StartEnd
from cm_pcmall.case.business.common.function import OperationMethod as om
from cm_pcmall.case.business.C_2_club_signed_in_business import ClubSignedInBusiness
from time import sleep


class ClubSignedInCase(StartEnd):
    def test_C01_select_my_beauty(self):
        '''
        检查'机构'登录，点击’我的采美‘是否跳转个人中心页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.select_my_beauty('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_01_select_my_beauty.png')
        self.assertEqual(scb.select_my_beauty('assert1'), '我的交易')
        self.assertEqual(scb.select_my_beauty('assert2'), '机构管理中心')
        self.assertEqual(scb.select_my_beauty(), '账户设置')

    def test_C02_select_club_information(self):
        '''
        检查'机构'登录，点击’机构资料‘是否跳转修改‘机构资料’页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.select_club_information('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_02_select_club_information.png')
        self.assertEqual(scb.select_club_information(), '机构管理中心 > 修改资料')

    def test_C03_select_log_out(self):
        '''
        检查'机构'登录，点击’退出登录‘是否正常退出登录
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.select_log_out('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_03_select_log_out.png')
        self.assertEqual(scb.select_log_out(), '登录')

    def  test_C04_shopping_cart(self):
        '''
        检查'机构'登录，点击’购物车（无商品）‘是否进入空购物车显示页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.shopping_cart('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_04_shopping_cart.png')
        self.assertEqual(scb.shopping_cart(), '购物车空空如也，快去商城，逛逛吧~')

    def test_C05_click_menu_my_beauty(self):
        '''
        检查'机构'登录，“个人中心”-点击菜单‘我的采美‘是否进入’我的采美‘显示页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_menu_my_beauty('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_05_click_menu_my_beauty.png')
        self.assertEqual(scb.click_menu_my_beauty(), '我的采美')

    def test_C06_upload_avatar(self):
        '''
        检查'机构'登录，“个人中心”-‘上传头像’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.upload_avatar('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_06_upload_avatar.png')
        self.assertEqual(scb.upload_avatar(), '我的采美')

    def test_C07_click_improve_immediately(self):
        '''
        检查'机构'登录，“个人中心”-点击‘立即完善’是否跳转‘完善资料’页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_improve_immediately('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_07_click_improve_immediately.png')
        self.assertEqual(scb.click_improve_immediately(), '机构管理中心 > 修改资料')

    def test_C08_click_my_order(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘是否进入’我的订单‘页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_my_order('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_08_click_my_order.png')
        self.assertEqual(scb.click_my_order(), '我的交易 > 我的订单')

    def test_C09_click_to_be_confirmed(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》’待确认‘是否进入’待确认‘tab
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_to_be_confirmed('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_09_click_to_be_confirmed.png')
        self.assertEqual(scb.click_to_be_confirmed(), '待确认')

    def test_C10_click_to_be_paid(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》’待付款‘是否进入’待付款‘tab
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_to_be_paid('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_10_click_to_be_paid.png')
        self.assertEqual(scb.click_to_be_paid(), '待付款部分发货')

    def test_C11_click_to_be_delivered(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》’待发货‘是否进入’待发货‘tab
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_to_be_delivered('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_11_click_to_be_delivered.png')
        self.assertEqual(scb.click_to_be_delivered(), '待付款部分发货')

    def test_C12_click_delivered(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》’已发货‘是否进入’已发货‘tab
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_delivered('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_12_click_delivered.png')
        self.assertEqual(scb.click_delivered(), '待付款部分发货')

    def test_C13_click_return_goods(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》’退货/款‘是否进入’退货/款‘tab
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_return_goods('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_13_click_return_goods.png')
        self.assertEqual(scb.click_return_goods(), '交易全退')

    def test_C14_click_contact_customer_service(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》’联系客服‘是否进入’联系客服页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_contact_customer_service('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_14_click_contact_customer_service.png')
        self.assertEqual(scb.click_contact_customer_service(), '帮助中心')

    def test_C15_order_number_search(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘订单编号搜索’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.order_number_search('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_15_order_number_search.png')
        self.assertEqual(scb.order_number_search(), '订单编号(标识)：W162097225336719(15637)')

    def test_C16_order_time(self):
        '''
.       检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘下单时间搜索’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.order_time('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_16_order_time.png')
        self.assertEqual(scb.order_time(), '订单编号(标识)：X161614199602056(15213)')

    def test_C17_view_Logistics(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘查看物流’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.view_Logistics('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_17_view_Logistics.png')
        self.assertEqual(scb.view_Logistics(), '物流详情')

    def test_C18_order_details(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘订单详情’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.order_details('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_18_order_details.png')
        self.assertEqual(scb.order_details(), '我的交易 > 我的订单 > 订单详情')

    def test_C19_payment_order(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘支付订单’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.payment_order('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_19_payment_order.png')
        self.assertEqual(scb.payment_order(), '待付金额：￥3,898.75')

    def test_C20_views_Logistics(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘’订单详情-》‘查看物流’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.views_Logistics('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_20_views_Logistics.png')
        self.assertEqual(scb.views_Logistics(), '物流详情')

    def test_C21_payment_orders(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的交易’-》‘我的订单‘-》‘’订单详情-》‘支付订单’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.payment_orders('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_21_payment_orders.png')
        self.assertEqual(scb.payment_orders(), '待付金额：￥3,898.75')

    def test_C22_click_my_maintenance(self):
        '''
        检查'机构'登录，“个人中心”-点击‘我的维修’是否进入’我的维修‘页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.click_my_maintenance('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_22_click_my_maintenance.png')
        self.assertEqual(scb.click_my_maintenance(), '我的交易 > 我的维修\n申请维修')

    def test_C23_apply_for_repair(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘我的维修’页面，操作‘申请维修’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.apply_for_repair('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_23_apply_for_repair.png')
        self.assertEqual(scb.apply_for_repair(), '维修申请提交成功！')

    def test_C24_key_word(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘我的维修’页面，‘关键词’搜索功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.key_word('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_24_key_word.png')
        self.assertEqual(scb.key_word(), '二维热无若')

    def test_C25_date_submission(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘我的维修’页面，‘提交日期’搜索功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.date_submission('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_25_date_submission.png')
        self.assertEqual(scb.date_submission(), '二维热无若')

    def test_C26_maintenance_progress(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘我的维修’页面，‘维修进度’搜索功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.maintenance_progress('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_26_dmaintenance_progress.png')
        self.assertEqual(scb.maintenance_progress(), '法诺智能美容')

    def test_C27_maintenance_details(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘我的维修’页面，点击进入‘维修详情’搜索功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.maintenance_details('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_27_maintenance_details.png')
        self.assertEqual(scb.maintenance_details(), '我的交易 > 我的维修 > 维修详情')

    def test_C28_new_address(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘收货地址管理’页面，‘新增地址’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.new_address('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_28_new_address.png')
        self.assertEqual(scb.new_address(), '保存成功')

    def test_C29_confirm_deletion(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘收货地址管理’页面，‘删除地址’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.confirm_deletion('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_29_confirm_deletion.png')
        self.assertEqual(scb.confirm_deletion(), '删除成功')

    def test_C30_institutional_information(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘机构资料’页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.institutional_information('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_30_institutional_information.png')
        self.assertEqual(scb.institutional_information(), '机构管理中心 > 修改资料')

    def test_C31_save_and_submit(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘机构资料’页面-‘修改机构资料’功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.save_and_submit('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_31_save_and_submit.png')
        self.assertEqual(scb.save_and_submit(), '修改成功')

    def test_C32_operator_management(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘运营人员管理‘页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.operator_management('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_32_operator_management.png')
        self.assertEqual(scb.operator_management(), '机构管理中心 > 运营人员管理')

    def test_C33_full_name_search(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘运营人员管理‘页面-'姓名'搜索功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.full_name_search('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_33_full_name_search.png')
        self.assertEqual(scb.full_name_search(), '小阿枫')

    def test_C34_cell_phone_number_search(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘运营人员管理‘页面-'手机号'搜索功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.cell_phone_number_search('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_34_cell_phone_number_search.png')
        self.assertEqual(scb.cell_phone_number_search(), '测试小屋')

    def test_C35_add_operators(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘运营人员管理‘页面-'添加运营人员'功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.add_operators('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_35_add_operators.png')
        self.assertEqual(scb.add_operators(), '添加成功')

    def test_C36_delete_operators(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘运营人员管理‘页面-'删除运营人员'功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.delete_operators('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_36_delete_operators.png')
        self.assertEqual(scb.delete_operators(), '删除成功')

    def test_C37_reset_password(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘重置密码‘页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.reset_password('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_37_reset_password.png')
        self.assertEqual(scb.reset_password(), '账户设置 > 重置密码')

    def test_C38_cell_phone_number(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘重置密码‘页面-'使用手机号修改密码'功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.cell_phone_number_modify('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_38_cell_phone_number.png')
        self.assertEqual(scb.cell_phone_number_modify(), '密码修改成功~')

    def test_C39_mailbox_modify(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘重置密码‘页面-'使用邮箱修改密码'功能是否正常
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.mailbox_modify('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_39_mailbox_modify.png')
        self.assertEqual(scb.mailbox_modify(), '密码修改成功~')

    def test_C40_change_mobile_phone_number(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘更换手机号‘页面
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.change_mobile_phone_number('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_40_change_mobile_phone_number.png')
        self.assertEqual(scb.change_mobile_phone_number(), '账户设置 > 更换手机号')

    def test_C41_successfully_replaced(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘更换手机号‘页面-’更换手机号‘功能是否正常,成功更换手机号成(19999999988)
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.successfully_replaced('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_41_successfully_replaced.png')
        self.assertEqual(scb.successfully_replaced(), '修改成功~')

    def test_C42_successfully_replaced_reduction(self):
        '''
        检查'机构'登录，“个人中心”-点击进入‘更换手机号‘页面-’更换手机号‘功能是否正常,成功更换手机号成(19999999998)
        :return:
        '''
        scb = ClubSignedInBusiness(self.driver)
        scb.successfully_replaced_reduction('action')
        sleep(2)
        om().screen_img(self.driver, 'C_2_42_successfully_replaced_reduction.png')
        self.assertEqual(scb.successfully_replaced(), '修改成功~')