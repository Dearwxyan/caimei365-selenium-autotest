#!/bin/bash
export LANG="en_US.UTF-8"
time=$(date)
echo "当前时间:"$time
echo "构建风格:自动化构建执行环境及执行自动化测试"
echo "项目名称:caimei365.com"
echo "执行人员:xiaoyan"

# 获取当前脚本存在路径（绝对路径）
project_dir=$(cd "$(dirname "$0")"; pwd)

# 更新‘pip’模块
if [ ! -d $project_dir/venv ]; then
  echo "step1：进行更新“pip”模块，用于安装“virtualenv”模块包。"
  python -m pip install --upgrade pip3 -i https://pypi.douban.com/simple
  # 安装wheel
  pip install wheel
else
  echo "step1：已更新“pip”模块，用于安装“virtualenv”模块包。"
fi

# 安装‘virtualenv’模块
if [ ! -d $project_dir/venv ]; then
  echo "step2：开始执行安装“virtualenv”模块包，用于创建“python”虚拟环境。"
  pip install virtualenv
else
  echo "step2：已安装“virtualenv”模块包，用于创建“python”虚拟环境。"
fi

# 检查项目是否存在虚拟环境,不存在创建虚拟环境
if [ ! -d $project_dir/venv ];then
  echo "step3：开始执行创建“python”虚拟环境。"
  virtualenv -p python $project_dir/venv
else
  echo "step3：“python”虚拟环境已创建。"
fi

#激活python虚拟环境
echo "step4：激活“python”虚拟环境。"
ve() { source $project_dir/venv/Scripts/activate; }

# 虚拟环境中安装模块包
if [ ! -x $project_dir/venv/Scripts/chromedriver.exe ]; then
  echo "step5：开始安装项目运行依赖包。"
  $project_dir/venv/Scripts/python -m pip install --upgrade pip -i https://pypi.douban.com/simple
  $project_dir/venv/Scripts/pip install -r $project_dir/requirements.txt
else
  echo "step5：已安装项目运行依赖包。"
fi

# 将驱动复制到虚拟环境执行目录
if [ ! -x $project_dir/venv/Scripts/chromedriver.exe ]; then
  echo "step6：进行复制“浏览器驱动”到虚拟环境执行目录。"
  cp -r  $project_dir/tool/browser_driver/* $project_dir/venv/Scripts/
else
  echo "step6：已复制“浏览器驱动”到虚拟环境执行目录。"
fi

# 将报告模板复制到虚拟环境执行目录
if [ ! -f $project_dir/venv/Lib/site-packages/test_HTMLTestReportCN.py ]; then
  echo "step7：进行复制“测试报告模板”到虚拟环境执行目录。"
  cp -r  $project_dir/tool/report_template1/* $project_dir/venv/Lib/site-packages/
else
  echo "step7：已复制“测试报告模板”到虚拟环境执行目录。"
fi

# 执行自动化测试
echo "step8：开始执行项目自动化测试。"
$project_dir/venv/Scripts/python runtest.py