# coding=utf-8
import os
import time
import unittest
from cm_pcmall.case.business.common.function import OperationMethod
from HTMLTestReportCN import HTMLTestRunner


class RunCase(object):
    def __init__(self):
        self.om = OperationMethod()
        self.pwd = os.getcwd()
        # self.base_dir = os.path.dirname(os.path.abspath(__file__))
        # self.base_dir = self.base_dir.replace('\\', '/')
        # self.base_dir = self.base_dir.split('/CAIMEI365-AUTOTEST')[0]
        # self.base_dir = self.base_dir + '/CAIMEI365-AUTOTEST'
        # self.base_dir = os.getcwd()
        # self.base_dir = self.base_dir.replace('\\', '/')
        self.base_dir = os.path.dirname(os.path.abspath(__file__))
        self.base_dir = self.base_dir.replace('\\', '/')
        self.now = time.strftime('%Y-%m-%d %H时%M分%S秒')

    # 运行case并生成报告
    def running(self, item):
        try:
            case_path = self.base_dir + '/case/'
            report_path = self.base_dir + '/report/'
            run_case = unittest.defaultTestLoader.discover(case_path, pattern='*case.py',
                                                           top_level_dir=self.base_dir)
            report_path = report_path + self.now + '[' + item + ']自动化测试报告.html'
            with open(report_path, 'wb') as f:
                runner = HTMLTestRunner(stream=f, title='[采美365]' + item + '自动化测试报告')
                runner.run(run_case)
                f.close()
        except BaseException as e:
            print("运行失败:" + str(e))

    # 创建截图、测试报告文件夹
    def mkdir(self, item):
        file_path = self.base_dir + '/report/'
        if not os.path.exists(file_path):
            try:
                report_path = self.base_dir + '/report/'
                screenshot_path = self.base_dir + '/case/business/screen_shot/'
                os.makedirs(report_path)
                os.makedirs(screenshot_path)
            except BaseException as e:
                print('创建失败：' + str(e))


if __name__ == '__main__':
    rc = RunCase()
    # 执行PC端测试
    rc.mkdir('cm_pcmall')
    rc.running('cm_pcmall')
    latest_report_path = rc.om.latest_report_path('cm_pcmall')
    read_report = rc.om.read_report(latest_report_path)
    add_annex = rc.om.add_annex(latest_report_path)
    rc.om.send_mail(read_report, add_annex)
    # 执行后台测试
    # rc.mkdir('cm_admin')
    # rc.running('cm_admin')
    # latest_report_path = rc.om.latest_report_path('cm_admin')
    # read_report = rc.om.read_report(latest_report_path)
    # add_annex = rc.om.add_annex(latest_report_path)
    # rc.om.send_mail(read_report, add_annex)
